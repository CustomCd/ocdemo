//
//  ViewController.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/14.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "ViewController.h"
#import <Masonry/Masonry.h>
#import "DocumentVC.h"
#import "GaoDeVC.h"
#import "EmojiVc.h"
#import <AFNetworking/AFNetworking.h>
#import "UpMoveVC.h"
#import "ChangeModelVC.h"
#import "ScrrenShotVC.h"
#import "DeviceOrientationVc.h"
#import "SystemShareVc.h"
#import "ShareDocumentVC.h"
#import "KVOVc.h"
#import "SelectMoreVc.h"
#import "AliPurchaseVc.h"
#define kSCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define kSCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define KIsIPhoneX ((kSCREEN_HEIGHT == 812.f || kSCREEN_HEIGHT == 896.f) ? YES : NO)

@interface ViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *tableview;
@property (nonatomic, strong) NSMutableArray *cell_arr;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.title = @"主页面";
    [self.view addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(KIsIPhoneX?-34:0);
    }];
    
}
\



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.cell_arr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.cell_arr[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.row) {
        case 0:
            [self enterFunctionVC:@"WChatVC"];
            break;
        case 1:
            [self enterFunctionVC:@"DocumentVC"];
            break;
        case 2:
            [self enterFunctionVC:@"GaoDeVC"];
            break;
        case 3:
            [self enterFunctionVC:@"EmojiVc"];
            break;
        case 4:
            [self enterFunctionVC:@"UpMoveVC"];
            break;
        case 5:
            [self enterFunctionVC:@"ChangeModelVC"];

            break;
        case 6:
            [self enterFunctionVC:@"ScrrenShotVC"];
            break;
        case 7:
            [self enterFunctionVC:@"DeviceOrientationVc"];
            break;
        case 8:
            [self enterFunctionVC:@"SystemShareVc"];
            break;
        case 9:
            [self enterFunctionVC:@"ShareDocumentVC"];
            break;
        case 10:
            [self enterFunctionVC:@"KVOVc"];
            break;
        case 11:
            [self enterFunctionVC:@"SelectMoreVc"];
            break;
        case 12:
            [self enterFunctionVC:@"AliPurchaseVc"];
            break;
        case 13:
            [self enterFunctionVC:@"WBQRCodeVC"];
            break;
        case 14:
            [self enterFunctionVC:@"RunLoopMainVc"];
            break;
        case 15:
            [self enterFunctionVC:@"ScrollViewVc"];
            break;
        case 16:
            [self enterFunctionVC:@"TimerToolVc"];
            break;
        case 17:
            [self enterFunctionVC:@"LoadGifVc"];
            break;
        case 18:
            [self enterFunctionVC:@"HideNavigationbarLineVc"];
            break;
        case 19:
            [self enterFunctionVC:@"OneBackgroudVc"];
            break;
        case 20:
            [self enterFunctionVC:@"PPTListVc"];
            break;
        case 21:
            [self enterFunctionVC:@"AnimationVc"];
            break;
    }
}
- (void)enterFunctionVC:(NSString *)class
{
    id myObj = [[NSClassFromString(class) alloc] init];
    
    [self.navigationController pushViewController:myObj animated:YES
     ];
}




//懒加载

- (UITableView *)tableview{
    if(!_tableview){
        _tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.rowHeight = 40;
            object.delegate = self;
            object.dataSource = self;
            
            object;
       });
    }
    return _tableview;
}

- (NSMutableArray *)cell_arr{
    if(!_cell_arr){
        _cell_arr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            [object addObject:@"微信登录"];
            [object addObject:@"加载文件"];
            [object addObject:@"高德地图"];
            [object addObject:@"Emoji表情"];
            [object addObject:@"上移"];
            [object addObject:@"改变model"];
            [object addObject:@"截屏保存"];
            [object addObject:@"旋转屏幕"];
            [object addObject:@"系统分享"];
            [object addObject:@"文件微信分享"];
            [object addObject:@"kvo 监听"];
            [object addObject:@"tablview 多选"];
            [object addObject:@"支付宝支付"];
            [object addObject:@"扫一扫"];
            [object addObject:@"Runloop"];
            [object addObject:@"滑动"];
            [object addObject:@"定时器"];
            [object addObject:@"加载 gif"];
            [object addObject:@"隐藏 navigationbar 黑线"];
            [object addObject:@"一张背景图多个机型使用"];
            [object addObject:@"录制并播放音频"];
            [object addObject:@"动画"];

            object;
       });
    }
    return _cell_arr;
}
@end
