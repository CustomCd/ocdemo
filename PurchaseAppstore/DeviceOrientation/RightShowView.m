//
//  RightShowView.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/26.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "RightShowView.h"
#import <Masonry/Masonry.h>
@implementation RightShowView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithWhite:0 alpha:0.8];
        UILabel *test = [[UILabel alloc] init];
        test.backgroundColor = [UIColor cyanColor];
        test.text = @"右侧滑出";
        [self addSubview:test];
        [test mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.top.bottom.equalTo(self);
            make.width.mas_equalTo(200);
        }];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickBtnAction)];
        [self addGestureRecognizer:tap];
    }
    return self;
}

- (void)clickBtnAction
{
    [UIView animateWithDuration:1 animations:^{
//        self.hidden = YES;

    }completion:^(BOOL finished) {
        if (self.dismissBlock) {
            self.dismissBlock();
            
        }
    }];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
