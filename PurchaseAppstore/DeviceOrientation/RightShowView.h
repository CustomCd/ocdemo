//
//  RightShowView.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/26.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^DismissBlock)(void);
@interface RightShowView : UIView
@property (nonatomic, copy) DismissBlock dismissBlock;

@end
