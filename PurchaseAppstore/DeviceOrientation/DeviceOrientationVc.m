//
//  DeviceOrientationVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/26.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "DeviceOrientationVc.h"
#import "AppDelegate.h"
#import <Masonry/Masonry.h>
#import "RightShowView.h"
#import <zhPopupController/zhPopupController.h>
@interface DeviceOrientationVc ()
{
    BOOL isfullScreen;
    BOOL click;
}
@property (nonatomic, strong) UIButton *clickBtn;
@property (nonatomic, strong) UIButton *showRightViewBtn;
@property (nonatomic, strong) RightShowView *rightShowView;

@end

@implementation DeviceOrientationVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.clickBtn];
    [self.clickBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(200, 80));
        make.centerY.equalTo(self.view.mas_centerY);
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    [self.view addSubview:self.showRightViewBtn];
    [self.showRightViewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    
    [self.view addSubview:self.rightShowView];
    [self.rightShowView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.bottom.top.equalTo(self.view);
    }];
    [self.view setNeedsLayout];
    [self.view layoutIfNeeded];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickButtonAction
{
    isfullScreen = !isfullScreen;
    AppDelegate *appdele = (AppDelegate *)[UIApplication sharedApplication].delegate;
    
    if (isfullScreen) {
        
        appdele.isSupportHori = YES;
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        //上一句话是防止手动先把设备置为横屏,导致下面的语句失效.
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIDeviceOrientationLandscapeRight] forKey:@"orientation"];
        
    }else{
        
        appdele.isSupportHori = NO;
        
        NSNumber *orientationUnknown = [NSNumber numberWithInt:UIDeviceOrientationUnknown];
        
        [[UIDevice currentDevice] setValue:orientationUnknown forKey:@"orientation"];
        [[UIDevice currentDevice] setValue:[NSNumber numberWithInteger:UIInterfaceOrientationPortrait] forKey:@"orientation"];
        
    }
}

- (void)clickRightButtonAction
{

    click = !click;
    CGFloat offset_x = click ?0:200;


    [UIView animateWithDuration:5 animations:^{
        [self.rightShowView mas_updateConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.view).offset(offset_x);

        }];
        [self.rightShowView layoutIfNeeded];
//        [self.view setNeedsLayout];
//        [self.view layoutIfNeeded];
//        self.rightShowView.hidden = !self.rightShowView.hidden;
        

    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (UIButton *)clickBtn{
    if(!_clickBtn){
        _clickBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"截屏" forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor cyanColor]];
            [object addTarget:self action:@selector(clickButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object;
        });
    }
    return _clickBtn;
}

- (UIButton *)showRightViewBtn{
    if(!_showRightViewBtn){
        _showRightViewBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"显示右边 view" forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor cyanColor]];
            [object addTarget:self action:@selector(clickRightButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _showRightViewBtn;
}

- (RightShowView *)rightShowView{
    if(!_rightShowView){
        _rightShowView = ({
            RightShowView * object = [[RightShowView alloc]init];
            __weak typeof(self)weakSelf = self;

            object.dismissBlock = ^{
//                [weakSelf clickButtonAction];
                [weakSelf clickRightButtonAction];
                
            };
//            object.hidden = YES;
            object;
       });
    }
    return _rightShowView;
}
@end
