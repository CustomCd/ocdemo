//
//  ChangeModelVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/10.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "ChangeModelVC.h"
#import <Masonry/Masonry.h>
#import "Person.h"
#import "NextVc.h"
@interface ChangeModelVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *model_tabelview;
@property (nonatomic, strong) NSMutableArray *data_list;

@end

@implementation ChangeModelVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.view addSubview:self.model_tabelview];
    [self.model_tabelview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.model_tabelview reloadData];
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data_list.count ;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    Person *person = self.data_list[indexPath.row];
    
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@\n%@", person.name,person.age];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Person *person = self.data_list[indexPath.row];
    NextVc *nextVc = [[NextVc alloc] init];
    nextVc.data_list = self.data_list;
    __weak typeof(self)weakSelf = self;

    nextVc.backBlock = ^(NSString *name) {
        person.name = name;
        [weakSelf.data_list replaceObjectAtIndex:indexPath.row withObject:person];
        [weakSelf.model_tabelview reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    };

    [self.navigationController pushViewController:nextVc animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)model_tabelview{
    if(!_model_tabelview){
        _model_tabelview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object;
       });
    }
    return _model_tabelview;
}

- (NSMutableArray *)data_list{
    if(!_data_list){
        _data_list = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            for (int i = 0; i<10; i++) {
                Person *person = [[Person alloc] init];
                person.name = [NSString stringWithFormat:@"测试名字%d",i];
                person.age = [NSString stringWithFormat:@"%d",i+10];
                [object addObject:person];
            }
            object;
       });
    }
    return _data_list;
}
@end
