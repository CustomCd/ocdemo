//
//  NextVc.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/10.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^BackBlock)(NSString *name);

@interface NextVc : UIViewController
@property (nonatomic, strong) NSMutableArray *data_list;
@property (nonatomic, assign) NSInteger row;
@property (nonatomic, copy) BackBlock backBlock;

@end
