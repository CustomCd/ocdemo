//
//  NextVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/10.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "NextVc.h"
#import <Masonry/Masonry.h>
#import "Person.h"
@interface NextVc ()
@property (nonatomic, strong) UIButton *testBtN;

@end

@implementation NextVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.testBtN];
    [self.testBtN mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 100));
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    
}

- (void)clickBtnAction
{
    Person *person = [[Person alloc] init];
    person.name = @"陈小东";
    person.age = @"28";
    self.backBlock(@"陈小东");
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIButton *)testBtN{
    if(!_testBtN){
        _testBtN = ({
            UIButton * object = [[UIButton alloc]init];
            object.backgroundColor = [UIColor cyanColor];
            [object addTarget:self action:@selector(clickBtnAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _testBtN;
}
@end
