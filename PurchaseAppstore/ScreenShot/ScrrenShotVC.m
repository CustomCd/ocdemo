//
//  ScrrenShotVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/9/26.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "ScrrenShotVC.h"
#import <Masonry/Masonry.h>
@interface ScrrenShotVC ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UIImageView *screenShot_img;
@property (nonatomic, strong) UIButton *clickBtn;
@property (nonatomic, strong) UITableView *shot_tableview;


@end

@implementation ScrrenShotVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.screenShot_img];
    
//    [self.screenShot_img mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.size.mas_equalTo(CGSizeMake(200, 200));
//        make.centerY.equalTo(self.view.mas_centerY);
//        make.centerX.equalTo(self.view.mas_centerX);
//    }];
    [self.view addSubview:self.shot_tableview];
    [self.shot_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self addRightShotButton];
}
- (void)addRightShotButton
{
    self.clickBtn.frame = CGRectMake(0, 0, 40, 40);
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.clickBtn];
}

- (void)clickButtonAction
{
    [self makeScreenShotCompletion:^(UIImage *image) {
        
    }];
}

-(void)makeScreenShotCompletion:(void(^)(UIImage * image))completion{
//    UIGraphicsBeginImageContext(self.view.bounds.size);//模糊
    UIImage* viewImage = nil;
    UITableView *scrollView = self.shot_tableview;
    UIGraphicsBeginImageContextWithOptions(scrollView.contentSize, scrollView.opaque, 0.0);
    {
        CGPoint savedContentOffset = scrollView.contentOffset;
        CGRect savedFrame = scrollView.frame;
        
        scrollView.contentOffset = CGPointZero;
        scrollView.frame = CGRectMake(0, 0, scrollView.contentSize.width, scrollView.contentSize.height);
        
        [scrollView.layer renderInContext: UIGraphicsGetCurrentContext()];
        viewImage = UIGraphicsGetImageFromCurrentImageContext();
        
        scrollView.contentOffset = savedContentOffset;
        scrollView.frame = savedFrame;
    }
    UIGraphicsEndImageContext();
    
    /**     *  将图片保存到本地相册     */
    UIImageWriteToSavedPhotosAlbum(viewImage, self , @selector(image:didFinishSavingWithError:contextInfo:), nil);//保存图片到照片库

}
/** *  图片保存到本地后的回调 */
- (void)image: (UIImage *) image didFinishSavingWithError: (NSError *) error contextInfo: (void *) contextInfo{
    if(!error){
        NSLog(@"保存成功");
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 50;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"海贼王%ld",indexPath.row];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIImageView *)screenShot_img{
    if(!_screenShot_img){
        _screenShot_img = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.image = [UIImage imageNamed:@"screenShot"];
            object;
       });
    }
    return _screenShot_img;
}

- (UIButton *)clickBtn{
    if(!_clickBtn){
        _clickBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"截屏" forState:UIControlStateNormal];
            [object setBackgroundColor:[UIColor cyanColor]];
            [object addTarget:self action:@selector(clickButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _clickBtn;
}

- (UITableView *)shot_tableview{
    if(!_shot_tableview){
        _shot_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object.rowHeight = 44;
            object;
       });
    }
    return _shot_tableview;
}
@end
