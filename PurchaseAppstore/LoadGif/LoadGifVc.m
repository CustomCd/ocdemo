//
//  LoadGifVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/11.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "LoadGifVc.h"
#import <UIImage+GIF.h>
#import <UIImageView+WebCache.h>
@interface LoadGifVc ()
@property (nonatomic, strong) UIImageView *gifImageview;
@property (nonatomic, assign) CGFloat img_size;

@end

@implementation LoadGifVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    // 读取gif图片数据

    //文件必须要 add 加进来才可以,拖进来不可以
    self.img_size = 100;
    NSString *path = [[NSBundle mainBundle] pathForResource:@"logo" ofType:@"gif"];
    [self.view addSubview:self.gifImageview];

    [self.gifImageview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.view).offset(100);
        make.size.mas_equalTo(CGSizeMake(self.img_size, self.img_size));

    }];

    [self.gifImageview sd_setImageWithURL:[NSURL URLWithString:@"http://img1.imgtn.bdimg.com/it/u=473895314,616407725&fm=206&gp=0.jpg"]];

   

}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    self.img_size = self.img_size == 100?200:100;
    [self.gifImageview mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(self.img_size,self.img_size));
    }];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIImageView *)gifImageview{
    if(!_gifImageview){
        _gifImageview = ({
            UIImageView * object = [[UIImageView alloc]init];
            object;
       });
    }
    return _gifImageview;
}
@end
