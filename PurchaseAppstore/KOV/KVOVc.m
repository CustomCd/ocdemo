//
//  KVOVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/11/6.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "KVOVc.h"
#import "KVOModel.h"
#import <Masonry/Masonry.h>
@interface KVOVc ()

@property (nonatomic, strong) KVOModel *kvoModel;
@property (nonatomic, strong) UIButton *kovBtn;

@end

@implementation KVOVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.kvoModel addObserver:self forKeyPath:@"age" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];
    [self.kvoModel addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld context:nil];

    [self.view addSubview:self.kovBtn];
    [self.kovBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.top.equalTo(self.view).offset(40+64+50);
        
    }];
}
- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context
{
    NSLog(@"改变===%@",change);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)clickKvoButtonAction:(UIButton *)button
{
    NSInteger age = [button.titleLabel.text integerValue];
    age += 1;
    [button setTitle:[NSString stringWithFormat:@"%ld",age] forState:UIControlStateNormal];
    self.kvoModel.age = button.titleLabel.text;
    self.kvoModel.name = [NSString stringWithFormat:@"名字+%@",button.titleLabel.text];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (KVOModel *)kvoModel{
    if(!_kvoModel){
        _kvoModel = ({
            KVOModel * object = [[KVOModel alloc]init];
            object.name = @"陈旭";
            object.age = @"18";
            object;
       });
    }
    return _kvoModel;
}

- (UIButton *)kovBtn{
    if(!_kovBtn){
        _kovBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"1" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor cyanColor];
            [object addTarget:self action:@selector(clickKvoButtonAction:) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _kovBtn;
}
@end
