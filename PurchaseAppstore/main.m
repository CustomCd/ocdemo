//
//  main.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/14.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
