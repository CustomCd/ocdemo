//
//  BaseDataModel.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "BaseDataModel.h"
#import <objc/runtime.h>

@implementation BaseDataModel
- (void)parseJsonData:(NSDictionary *)dictionary
{
}

- (NSMutableArray *)getProperties:(Class)cls{
    unsigned int count;
    objc_property_t *properties = class_copyPropertyList(cls, &count);
    NSMutableArray *mArray = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        objc_property_t property = properties[i];
        const char *cName = property_getName(property);
        NSString *name = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
        [mArray addObject:name];
    }
    
    return mArray.copy;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        Ivar ivar = ivars[i];
        const char *name = ivar_getName(ivar);
        NSString *key = [NSString stringWithUTF8String:name];
        if ([key isEqualToString:@"_task"]) {
            continue;
        }
        id value = [self valueForKey:key];
        if (!value) {
            continue;
        }
        [aCoder encodeObject:value forKey:key];
    }
    free(ivars);
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        unsigned int count = 0;
        Ivar *ivars = class_copyIvarList([self class], &count);
        for (int i = 0; i<count; i++) {
            Ivar ivar = ivars[i];
            const char *name = ivar_getName(ivar);
            NSString *key = [NSString stringWithUTF8String:name];
            if ([key isEqualToString:@"_task"]) {
                continue;
            }
            id value = [aDecoder decodeObjectForKey:key];
            if (!value) {
                continue;
            }
            [self setValue:value forKey:key];
        }
        free(ivars);
    }
    return self;
}
@end
