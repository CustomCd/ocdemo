//
//  PaiAudioPlayerEngine
//  snsktv
//
//  Created by Pai on 2017/5/17.
//  Copyright © 2017年 HOME Ma. All rights reserved.
//

#import "PaiAudioPlayerEngine.h"

#define kNCMusicEngineCacheFolderName @"com.nickcheng.NCMusicEngine"
#define kNCMusicEngineCheckMusicInterval 0.1f // Seconds
#define kNCMusicEngineSizeBuffer 100000.0f // Bytes
#define kNCMusicEnginePauseMargin 1.0f  // Seconds
#define kNCMusicEnginePlayMargin 5.0f // Seconds
#define KCurrentTimeRecoder @"CurrentTimeRecoder"
#pragma mark PaiAudioPlayerEngine
@interface PaiAudioPlayerEngine()<PaiAudioPlayerAVPlayerDelegate,PaiAudioPlayerAudioSessionDelegate>

@property (nonatomic,strong) PaiAudioPlayerAVPlayer *AVPlayer;
@property (nonatomic,strong) PaiAudioPlayerAudioSession *SessonPlayer;
@end

@implementation PaiAudioPlayerEngine

+(instancetype)shareInstance
{
    static dispatch_once_t onceToken;
    static id instance;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc ] init];
    });
    return instance;
}
#pragma mark lazy load method
//-(PaiAudioPlayerAudioSession *)SessonPlayer{
//    if (!_SessonPlayer) {
//        _SessonPlayer = [[PaiAudioPlayerAudioSession alloc] init];
//    }
//    return _SessonPlayer;
//}
//-(PaiAudioPlayerAVPlayer *)AVPlayer{
//    if (!_AVPlayer) {
//        _AVPlayer = [[PaiAudioPlayerAVPlayer alloc] init];
//    }
//    return _AVPlayer;
//}

-(void)playMusicWithPath:(NSString *)filepath isVideo:(BOOL)isVideo onView:(UIView *)view
{
    if (![[NSFileManager defaultManager] fileExistsAtPath:filepath]) {
        if (!isVideo) {// 在线音乐播放
            if (self.SessonPlayer) {
                [self.SessonPlayer stop];
                self.SessonPlayer = nil;
            }
            if (!self.AVPlayer)
            {
                self.AVPlayer = [[PaiAudioPlayerAVPlayer alloc] init];
            }
            self.AVPlayer.delegate = self;
            [self.AVPlayer voicePalyerWith:filepath isVideo:isVideo onView:nil];
        }
        else{// 在线视频播放
            if (self.SessonPlayer) {
                [self.SessonPlayer stop];
                self.SessonPlayer = nil;
            }
            if (!self.AVPlayer)
            {
                self.AVPlayer = [[PaiAudioPlayerAVPlayer alloc] init];
            }
            self.AVPlayer.delegate = self;
            [self.AVPlayer voicePalyerWith:filepath isVideo:isVideo onView:view];
        }
    }
    else{
        if (!isVideo) {// 本地音乐播放
            if (self.AVPlayer) {
                [self.AVPlayer pause];
                self.AVPlayer = nil;
            }
            if (!self.SessonPlayer) {
                self.SessonPlayer = [[PaiAudioPlayerAudioSession alloc] init];
                
            }
            self.SessonPlayer.delegate = self;
            [self.SessonPlayer playWithFilePath:filepath];
        }
        else{// 本地视频
            if (self.SessonPlayer) {
                [self.SessonPlayer stop];
                self.SessonPlayer = nil;
            }
            if (!self.AVPlayer)
            {
                self.AVPlayer = [[PaiAudioPlayerAVPlayer alloc] init];
                
            }
            self.AVPlayer.delegate = self;
            [self.AVPlayer voicePalyerWith:filepath isVideo:isVideo onView:view];
        }
    }
}


- (void)pause
{
    if (self.AVPlayer) {
        [self.AVPlayer pause];
    }
    if (self.SessonPlayer) {
        [self.SessonPlayer pause];
    }
}

- (void)resume
{
    if (self.AVPlayer) {
        [self.AVPlayer play];
    }
    if (self.SessonPlayer) {
        [self.SessonPlayer resume];
    }
}

- (void)stop
{
    if (self.AVPlayer) {
        [self.AVPlayer pause];
    }
    if (self.SessonPlayer) {
        [self.SessonPlayer stop];
    }
}

- (void)playAtTime:(NSTimeInterval)time
{
    if (self.AVPlayer) {
        [self.AVPlayer seekToTime:time];
    }
    if (self.SessonPlayer) {
        [self.SessonPlayer playAtTime:time];
    }
}

- (NSTimeInterval)getPlayDurationTime
{
    if (self.AVPlayer) {
        return  [self.AVPlayer getAVPlayerDurationTime];
    }
    else{
        return [self.SessonPlayer getPlayDurationTime];
    }
}

- (NSTimeInterval)getPlayCurrentTime
{
    if (self.AVPlayer) {
        return  [self.AVPlayer getAvPlayerCurrentTime];
    }
    else{
        return [self.SessonPlayer getPlayCurrentTime];
    }
}

- (float)getPlayRate
{
    if (self.AVPlayer) {
        return  [self.AVPlayer getAvPlayerRate];
    }
    else{
        return [self.SessonPlayer getPlayRate];
    }
}

- (void)closeEnginePlayer
{
    if (self.AVPlayer) {
        [self.AVPlayer closeAVPlayer];
    }
    else{
        [self.SessonPlayer closeAudioPlayer];
    }
}
#pragma mark PaiAudioPlayerAVPlayerDelegate method
- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer updateBufferProgress:(NSTimeInterval)progress isCanPlay:(BOOL)isCanPlay{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:bufferProgress:isCanPlay:)]) {
        [self.delegate engine:avplayer bufferProgress:progress isCanPlay:isCanPlay];
    }
}

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer updatePlayerTime:(NSTimeInterval)time DurationTime:(NSTimeInterval)duration{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:playerCurrentTime:durationTime:)]) {
        [self.delegate engine:avplayer playerCurrentTime:time durationTime:duration];
        [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithFloat:time] forKey:KCurrentTimeRecoder];
    }
}

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerDidFinished:(BOOL)isSuccessfully{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didFinishedSuccessfully:)]) {
        [self.delegate engine:avplayer didFinishedSuccessfully:isSuccessfully];
    }
}

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerDidError:(NSError *)error{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didPlayMusicFailed:)]) {
        [self.delegate engine:avplayer didPlayMusicFailed:error];
    }
}

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerStatusChange:(MyAVPlayerStatus)playerStatus{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didChangeEngineStatus:)]) {
        [self.delegate engine:avplayer didChangeEngineStatus:(EnginePlayerStatus)playerStatus];
    }
}

#pragma mark PaiAudioPlayerAudioSessionDelegate method
- (void)engine:(PaiAudioPlayerAudioSession *)engine didChangePlayState:(MyAudioSessionState)playState;
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didChangeEngineStatus:)]) {
        [self.delegate engine:engine didChangeEngineStatus:(EnginePlayerStatus)playState];
    }
}
- (void)engine:(PaiAudioPlayerAudioSession *)engine downloadProgress:(CGFloat)progress
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:bufferProgress:isCanPlay:)]) {
        [self.delegate engine:engine bufferProgress:progress isCanPlay:YES];
    }
}
- (void)engine:(PaiAudioPlayerAudioSession *)engine playCurrentTime:(NSTimeInterval)currentTime playDuration:(NSTimeInterval)duration
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:playerCurrentTime:durationTime:)]) {
        [self.delegate engine:engine playerCurrentTime:currentTime durationTime:duration];
    }
}
- (void)engineDidFinishPlaying:(PaiAudioPlayerAudioSession *)engine successfully:(BOOL)flag
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didFinishedSuccessfully:)]) {
        [self.delegate engine:engine didFinishedSuccessfully:flag];
    }
}

- (void)engine:(PaiAudioPlayerAudioSession *)engine playFail:(NSError *)error
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didPlayMusicFailed:)]) {
        [self.delegate engine:engine didPlayMusicFailed:error];
    }
}
#pragma mark Engine dealloc
-(void)dealloc
{
    NSLog(@"-----%@-----%@",self.SessonPlayer,self.AVPlayer);
}
@end
#pragma mark PaiAudioPlayerAudioSession
@interface PaiAudioPlayerAudioSession () <AVAudioPlayerDelegate>

@property (nonatomic, assign, readwrite) MyAudioSessionState playState;
@property (nonatomic, strong, readwrite) NSError *error;
@property (nonatomic, strong) AVAudioPlayer *player;

- (void)playLocalFile;
@end

@implementation PaiAudioPlayerAudioSession
{
    NSString *_localFilePath;
    NSTimer *_playCheckingTimer;
}
- (id)init {
    return [self initWithSetBackgroundPlaying:YES];
}

- (id)initWithSetBackgroundPlaying:(BOOL)setBGPlay {
    //
    self = [super init];
    if(self){
        
        if (setBGPlay) {
            if ([[[AVAudioSession sharedInstance]category ]isEqualToString:AVAudioSessionCategoryPlayAndRecord]) {
                
            }else
            {
                [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
                [[AVAudioSession sharedInstance] setActive: YES error: nil];
            }
            //[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
        }
    }
    return self;
}
- (void)playWithFilePath:(NSString *)filePath{
    
    self.playState = AudioSessionStateStopped;
    
    _localFilePath = filePath;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(engine:downloadProgress:)]) {
            [self.delegate engine:self downloadProgress:1.0];
        }
        
        [self playLocalFile];
    }
}

- (void)playLocalFile
{
    
    if (!self.player) {
        NSURL *musicUrl = [[NSURL alloc] initFileURLWithPath:_localFilePath isDirectory:NO];
        NSError *error = nil;
        self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:musicUrl error:&error];
        self.player.delegate = self;
        if (error) {
            NSLog(@"[NCMusicEngine] AVAudioPlayer initial error: %@", error);

            self.error = error;
            self.playState = AudioSessionStateError;
        }
    }
    
    if (self.player) {
        if (!self.player.isPlaying) {
            
            if ([self.player prepareToPlay]) NSLog(@"prepareToPlay");
            if ([self.player play]) NSLog(@"play");
            
            self.playState = AudioSessionStatePlaying;
            
            [self startPlayCheckingTimer];
        }
    }
}

- (void)startPlayCheckingTimer {
    //
    if (_playCheckingTimer) {
        [_playCheckingTimer invalidate];
        _playCheckingTimer = nil;
    }
    
    _playCheckingTimer = [NSTimer scheduledTimerWithTimeInterval:kNCMusicEngineCheckMusicInterval
                                                          target:self
                                                        selector:@selector(handlePlayCheckingTimer:)
                                                        userInfo:nil
                                                         repeats:YES];
}
- (void)handlePlayCheckingTimer:(NSTimer *)timer {
    //
    NSTimeInterval playerCurrentTime = self.player.currentTime;
    NSTimeInterval playerDuration = [self getPlayDurationTime];//self.player.duration;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:playCurrentTime:playDuration:)]) {
        if (playerDuration <= 0)
            [self.delegate engine:self playCurrentTime:playerCurrentTime playDuration:playerDuration];
        else
            [self.delegate engine:self playCurrentTime:playerCurrentTime playDuration:playerDuration];
    }

    playerDuration = self.player.duration;
    if (playerDuration - playerCurrentTime < kNCMusicEnginePauseMargin ) {
        [self pause];
        
    }
}
-(void)setPlayState:(MyAudioSessionState)playState
{
    _playState = playState;
    if (self.delegate && [self.delegate respondsToSelector:@selector(engine:didChangePlayState:)]) {
        [self.delegate engine:self didChangePlayState:_playState];
    }
}
- (void)pause {
    if (self.player && self.player.isPlaying) {
        [self.player pause];
        self.playState = AudioSessionStatePaused;
        if (_playCheckingTimer) {
            [_playCheckingTimer invalidate];
            _playCheckingTimer = nil;
        }
    }
}

- (void)resume {
    if (self.player && !self.player.isPlaying) {
        [self.player play];
        self.playState = AudioSessionStatePlaying;
        [self startPlayCheckingTimer];
    }
}

- (void)stop {
    
    if (self.player) {
        [self.player stop];
        self.playState = AudioSessionStateStopped;
        if (_playCheckingTimer) {
            [_playCheckingTimer invalidate];
            _playCheckingTimer = nil;
        }
    }
}

- (void)playAtTime:(NSTimeInterval)time
{
    if (self.player) {
        [self.player prepareToPlay];
        NSTimeInterval playerDuration = self.player.duration;
        if (time < playerDuration) {
            [self.player setCurrentTime:time];
            [self resume];
        }
        
    }
}
- (void)audioPlayerDidFinishPlaying
{
    [self stop];
    self.playState = AudioSessionStateEnded;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(engineDidFinishPlaying:successfully:)]) {
        [self.delegate engineDidFinishPlaying:self successfully:YES];
    }
}
- (NSTimeInterval)getPlayDurationTime
{
    if (self.player) {
        return self.player.duration;
    }
    return 0;
}

- (float)getPlayRate
{
    if (self.player) {
        return self.player.rate;
    }
    return 0;
}
- (NSTimeInterval)getPlayCurrentTime
{
    if (self.player) {
        return self.player.currentTime;
    }
    return 0;
    
}
#pragma mark AVAudioPlayerDelegate
- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    if (flag) {
        [self audioPlayerDidFinishPlaying];
    }
}


/* if an error occurs while decoding it will be reported to the delegate. */
- (void)audioPlayerDecodeErrorDidOccur:(AVAudioPlayer *)player error:(NSError *)error
{
    
}

- (void)audioPlayerBeginInterruption:(AVAudioPlayer *)player
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engineBeginInterruptionPlaying:)]) {
        [self.delegate engineBeginInterruptionPlaying:self];
    }
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withOptions:(NSUInteger)flags
{
    [self endInterruption];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player withFlags:(NSUInteger)flags
{
    [self endInterruption];
}

- (void)audioPlayerEndInterruption:(AVAudioPlayer *)player
{
    [self endInterruption];
}

- (void)endInterruption
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(engineEndInterruptionPlaying:)]) {
        [self.delegate engineEndInterruptionPlaying:self];
    }
    
}
- (void)closeAudioPlayer
{
    
//    [self pause];
    //[[AVAudioSession sharedInstance] setActive: NO error: nil];
    [[UIApplication sharedApplication] endReceivingRemoteControlEvents];
    if (self.player) {
        [self.player stop];
        self.player = nil;
    }
}
#pragma mark Audio dealloc
-(void)dealloc
{
    [self closeAudioPlayer];
}
@end

#pragma mark PaiAudioPlayerAVPlayer
@interface PaiAudioPlayerAVPlayer()
@property (nonatomic, strong)AVPlayer *player;
@property (nonatomic, strong)AVAsset *assetPlayer;
@property (nonatomic, strong)id timeObserver;
@end


@implementation PaiAudioPlayerAVPlayer
{

    BOOL m_isPlaying;
    MyAVPlayerStatus myState;

}

-(AVPlayer *)player
{

    if (!_player) {
        _player = [[AVPlayer alloc] init];
        if([[UIDevice currentDevice] systemVersion].intValue>=10){
            _player.automaticallyWaitsToMinimizeStalling = NO;
        }
    }

    return _player;

}
- (void)voicePalyerWith:(NSString *)filePath isVideo:(BOOL)isVideo onView:(UIView *)view{
    
    
       [[AVAudioSession sharedInstance]setCategory:AVAudioSessionCategoryPlayAndRecord withOptions:AVAudioSessionCategoryOptionDefaultToSpeaker error:nil];
       [[AVAudioSession sharedInstance] setActive: YES error: nil];

        //[[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    

    
    [self currentItemPlayerRemoveObserver];
    BOOL isLocal = NO;
    if ([[NSFileManager defaultManager] fileExistsAtPath:filePath]) {// 本地
        isLocal = YES;
    }
    if (!isVideo) {
        
        NSURL *soundUrl =isLocal? [NSURL fileURLWithPath:filePath]:[NSURL URLWithString:filePath];
        self.assetPlayer = [AVURLAsset URLAssetWithURL:soundUrl options:nil];
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithAsset:_assetPlayer];
        self.assetPlayer = [AVURLAsset URLAssetWithURL:soundUrl options:nil];
        [self.player replaceCurrentItemWithPlayerItem:playerItem];
        
    }
    else{
        
        NSURL *videoUrl = isLocal ? [NSURL fileURLWithPath:filePath]: [NSURL URLWithString:filePath];
        
        self.assetPlayer = [AVURLAsset URLAssetWithURL:videoUrl options:nil];
        AVPlayerItem *assetItem = [AVPlayerItem playerItemWithAsset:_assetPlayer];
        [self.player replaceCurrentItemWithPlayerItem:assetItem];
        AVPlayerLayer *playerLayer =[AVPlayerLayer playerLayerWithPlayer:_player];
        [playerLayer setFrame:view.bounds];
        playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        [view.layer addSublayer:playerLayer];
        
    }
    // 断点续播
    NSNumber *seekToTime = (NSNumber *)[[NSUserDefaults standardUserDefaults] objectForKey:KCurrentTimeRecoder];
    NSTimeInterval timeInterval;
    if (seekToTime == nil) {
        timeInterval = 0;
    }
    else{
#pragma mark 这里不处理是因为具体还需要判断，这里只是提供一个断点续播的思路
        timeInterval = 0;
//        timeInterval = seekToTime.floatValue;
    }
    myState = AVPlayerStatusUnknowns;
    [self.player seekToTime:CMTimeMake(timeInterval, 1)];
    [self currentItemPlayerAddObserver];
    
}

-(void)seekToTime:(NSTimeInterval)sekToTime
{
    if (self.player) {
        [self.player seekToTime:CMTimeMake(sekToTime, 1)];
        [self play];
    }
}
- (void)setVolume:(double)volume
{
//    NSMutableArray *allAudioParams = [NSMutableArray array];
//    AVMutableAudioMixInputParameters *audioInputParams =[AVMutableAudioMixInputParameters audioMixInputParameters];
//    [audioInputParams setVolume:volume atTime:kCMTimeZero];
//    [audioInputParams setTrackID:1];
//    [allAudioParams addObject:audioInputParams];
//    AVMutableAudioMix* audioMix = [AVMutableAudioMix audioMix];
//    [audioMix setInputParameters:allAudioParams];
//    [self.player.currentItem setAudioMix:audioMix];
    [self.player setVolume:volume];
}
- (void)closeAVPlayer{
    
    [self pause];
    [self currentItemPlayerRemoveObserver];
    
}

- (void)currentItemPlayerRemoveObserver{
    @try {
        [self.player.currentItem removeObserver:self forKeyPath:@"status"];
        [self.player.currentItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
        //[self.player.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
        //[self.player.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
        [self.player removeTimeObserver:self.timeObserver];
    }
    @catch (NSException *exception) {
        NSLog(@"多次删除了");
    }
    
}

- (void)currentItemPlayerAddObserver{
    @try {

    [self.player.currentItem addObserver:self forKeyPath:@"status" options:(NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew) context:nil];
    //监控缓冲加载情况属性
    [self.player.currentItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    // 缓冲区空了，需要等待数据
    //[self.player.currentItem addObserver:self forKeyPath:@"playbackBufferEmpty" options: NSKeyValueObservingOptionNew context:nil];
    // 缓冲区有足够数据可以播放了
    //[self.player.currentItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options: NSKeyValueObservingOptionNew context:nil];
    
    //监控播放完成通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:self.player.currentItem];
    }
    @catch (NSException *exception) {
        NSLog(@"多次添加了");
    }
    //监控时间进度
    __weak typeof(self) wself = self;
    
    self.timeObserver = [self.player addPeriodicTimeObserverForInterval:CMTimeMake(1, 30) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        
        if (wself.delegate && [wself.delegate respondsToSelector:@selector(avplayer:updatePlayerTime:DurationTime:)]) {
            [wself.delegate avplayer:wself updatePlayerTime:CMTimeGetSeconds(wself.player.currentItem.currentTime)  DurationTime:CMTimeGetSeconds(wself.player.currentItem.asset.duration)];
        }
    }];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    AVPlayerItem *playerItem = object;
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerItemStatus status = (AVPlayerItemStatus)[change[@"new"] integerValue];
        switch (status) {
            case AVPlayerItemStatusReadyToPlay:
            {
                // 开始播放
                if (![change[@"new"] isEqual:change[@"old"]]) {
                    sleep(0.5);
                    [self play];
//                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_global_queue(0, 0) , ^{
//                        [self play];
//                    });
                    
                }
            }
                break;
            case AVPlayerItemStatusFailed:
            {
                myState = AVPlayerStatusError;
                if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerDidError:)]) {
                    [self.delegate avplayer:self avPlayerStatusChange:myState];
                }
            }
                break;
            case AVPlayerItemStatusUnknown:
            {
                myState = AVPlayerStatusUnknowns;
                if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerStatusChange:)]) {
                    [self.delegate avplayer:self avPlayerStatusChange:myState];
                }
            }
                break;
            default:
                break;
        }
    }
    else if([keyPath isEqualToString:@"loadedTimeRanges"]){
        NSArray *array=playerItem.loadedTimeRanges;
        //本次缓冲时间范围
        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        //缓冲总长度
        NSTimeInterval totalBuffer = startSeconds + durationSeconds;
        BOOL isCanPlay ;
        if (totalBuffer < [self getAvPlayerCurrentTime] + 10) {
            isCanPlay = NO;
        }
        else {
            isCanPlay = YES;
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:updateBufferProgress:isCanPlay:)])
        {
            [self.delegate avplayer:self updateBufferProgress:totalBuffer  isCanPlay:isCanPlay];
        }
        
    }
    else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) {
        myState = AVPlayerStatusPaused;
        if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerStatusChange:)]) {
            [self.delegate avplayer:self avPlayerStatusChange:myState];
        }
    }
    else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        if (myState != AVPlayerStatusPaused && myState!= AVPlayerStatusEnded) {
            myState = AVPlayerStatusPlaying;
            if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerStatusChange:)]) {
                [self.delegate avplayer:self avPlayerStatusChange:myState];
            }
        }
    }
    
}


- (void)playbackFinished:(NSNotification *)notify {
    NSLog(@"播放完成");
    if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerDidFinished:)]) {
        [self.delegate avplayer:self avPlayerDidFinished:YES];
    }
    
}

- (void)play {
    
    if (self.player.status == AVPlayerStatusReadyToPlay && self.player &&myState != AVPlayerStatusPlaying) {
        
        [self.player play];
        myState = AVPlayerStatusPlaying;
        if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerStatusChange:)]) {
            [self.delegate avplayer:self avPlayerStatusChange:AVPlayerStatusPlaying];
        }
        m_isPlaying = YES;
    }
}

- (void)pause {
    
    if (self.player.status == AVPlayerStatusReadyToPlay && self.player &&myState != AVPlayerStatusPaused) {
        
        [self.player pause];
        myState = AVPlayerStatusPaused;
        if (self.delegate && [self.delegate respondsToSelector:@selector(avplayer:avPlayerStatusChange:)]) {
            [self.delegate avplayer:self avPlayerStatusChange:AVPlayerStatusPaused];
        }
        m_isPlaying = NO;
    }
}

- (BOOL)isPlayingStatus{
    return m_isPlaying;
}
-(CGFloat)getAvPlayerRate
{
    if ([self systemVersionUp: 10.0]) {
        return (float)self.player.timeControlStatus;
    }
    return self.player.rate;
}
-(NSTimeInterval)getAvPlayerCurrentTime
{
    CMTime cmtime = self.player.currentItem.currentTime;
    if (cmtime.timescale > 0 ) {
        return CMTimeGetSeconds(self.player.currentItem.currentTime);
    }
    return 0;
}
- (NSTimeInterval)getAVPlayerDurationTime{
    return CMTimeGetSeconds(self.player.currentItem.asset.duration);
}
-(BOOL)systemVersionUp:(CGFloat)version
{
    BOOL isFlag = NO;
    if([UIDevice currentDevice].systemVersion.floatValue >= version)
    {
        isFlag = YES;
    }
    return isFlag;
}
#pragma mark AVPlayer dealloc
-(void)dealloc
{
    [self closeAVPlayer];
}
@end
