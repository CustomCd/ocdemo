//
//  RecordModel.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/11.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface RecordModel : NSObject
@property (nonatomic, copy) NSString *record_title;
@property (nonatomic, copy) NSString *record_path;
@property (nonatomic, copy) NSString *record_time;
@property (nonatomic, assign) NSInteger *record_num;






@end

NS_ASSUME_NONNULL_END
