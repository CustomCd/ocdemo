//
//  PaiAudioPlayerEngine
//  snsktv
//
//  Created by Pai on 2017/5/17.
//  Copyright © 2017年 HOME Ma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <UIKit/UIKit.h>

@class PaiAudioPlayerAudioSession;
@class PaiAudioPlayerAVPlayer;

typedef NS_ENUM(NSInteger,EnginePlayerStatus)  {
    EnginePlayerStatusStopped,
    EnginePlayerStatusPlaying,
    EnginePlayerStatusPaused,
    EnginePlayerStatusEnded,
    EnginePlayerStatusError,
    EnginePlayerStatusUnknowns,
};

@protocol PaiAudioPlayerEngineDelegate <NSObject>

@optional
- (void)engine:(NSObject *)player didChangeEngineStatus:(EnginePlayerStatus)EngineStaus;

- (void)engine:(NSObject *)player bufferProgress:(NSTimeInterval)progress isCanPlay:(BOOL)isCanPlay;

- (void)engine:(NSObject *)player playerCurrentTime:(NSTimeInterval)current durationTime:(NSTimeInterval)durationTime;

- (void)engine:(NSObject *)player didFinishedSuccessfully:(BOOL)isSuccessfully;

- (void)engine:(NSObject *)player didPlayMusicFailed:(NSError *)error;


@end

@interface PaiAudioPlayerEngine : NSObject

+(instancetype)shareInstance;

-(void)playMusicWithPath:(NSString *)filepath isVideo:(BOOL)isVideo onView:(UIView *)view;

@property (nonatomic,weak) id<PaiAudioPlayerEngineDelegate> delegate;

- (void)pause;

- (void)resume;

- (void)stop;

- (void)playAtTime:(NSTimeInterval)time;

- (NSTimeInterval)getPlayDurationTime;

- (NSTimeInterval)getPlayCurrentTime;

- (float)getPlayRate;

- (void)closeEnginePlayer;
@end


typedef NS_ENUM(NSInteger,  MyAudioSessionState)
{
    AudioSessionStateStopped,
    AudioSessionStatePlaying,
    AudioSessionStatePaused,
    AudioSessionStateEnded,
    AudioSessionStateError,
    AudioSessionStateUnknowns,
};



@protocol PaiAudioPlayerAudioSessionDelegate <NSObject>

@optional
- (void)engine:(PaiAudioPlayerAudioSession *)engine didChangePlayState:(MyAudioSessionState)playState;
- (void)engine:(PaiAudioPlayerAudioSession *)engine downloadProgress:(CGFloat)progress;
- (void)engine:(PaiAudioPlayerAudioSession *)engine playCurrentTime:(NSTimeInterval)currentTime playDuration:(NSTimeInterval)duration;
- (void)engineDidFinishPlaying:(PaiAudioPlayerAudioSession *)engine successfully:(BOOL)flag;
- (void)engineBeginInterruptionPlaying:(PaiAudioPlayerAudioSession *)engine;
- (void)engineEndInterruptionPlaying:(PaiAudioPlayerAudioSession *)engine;
- (void)engine:(PaiAudioPlayerAudioSession *)engine playFail:(NSError *)error;
@end

@interface PaiAudioPlayerAudioSession : NSObject
@property (nonatomic, assign, readonly) MyAudioSessionState playState;
@property (nonatomic, strong, readonly) NSError *error;
@property (nonatomic, assign) id<PaiAudioPlayerAudioSessionDelegate> delegate;
@property (nonatomic, assign) CGFloat voiceFileLength;


- (id)initWithSetBackgroundPlaying:(BOOL)setBGPlay;

- (void)playWithFilePath:(NSString *)filePath;

- (void)pause;

- (void)resume;

- (void)stop;

- (void)playAtTime:(NSTimeInterval)time;

- (NSTimeInterval)getPlayDurationTime;

- (NSTimeInterval)getPlayCurrentTime;

- (float)getPlayRate;

- (void)closeAudioPlayer;
@end


typedef NS_ENUM(NSInteger, MyAVPlayerStatus) {
    AVPlayerStatusStopped,
    AVPlayerStatusPlaying,
    AVPlayerStatusPaused,
    AVPlayerStatusEnded,
    AVPlayerStatusError,
    AVPlayerStatusUnknowns,
};

@protocol PaiAudioPlayerAVPlayerDelegate <NSObject>

@optional

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer updateBufferProgress:(NSTimeInterval)progress isCanPlay:(BOOL)isCanPlay;

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer updatePlayerTime:(NSTimeInterval)time DurationTime:(NSTimeInterval)duration;

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerDidFinished:(BOOL)isSuccessfully;

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerDidError:(NSError *)error;

- (void)avplayer:(PaiAudioPlayerAVPlayer *)avplayer avPlayerStatusChange:(MyAVPlayerStatus)playerStatus;

@end

@interface PaiAudioPlayerAVPlayer : NSObject

@property (nonatomic, assign) id<PaiAudioPlayerAVPlayerDelegate> delegate;

- (void)voicePalyerWith:(NSString *)filePath isVideo:(BOOL)isVideo onView:(UIView *)view;

- (void)play;

- (void)pause;

-(void)seekToTime:(NSTimeInterval)sekToTime;

-(NSTimeInterval)getAvPlayerCurrentTime;

-(CGFloat)getAvPlayerRate;

- (BOOL)isPlayingStatus;

- (NSTimeInterval)getAVPlayerDurationTime;

- (void)closeAVPlayer;

@end

