//
//  PPTInfoModel.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "PPTInfoModel.h"

@implementation PPTInfoModel
- (void)parseJsonData:(NSDictionary *)dictionary
{
    self.ppt_id = SAFESTRING(dictionary[@"id"]);
    self.ppt_url = SAFESTRING(dictionary[@"attachmentUrl"]);
    self.ppt_time = SAFESTRING(dictionary[@"updatedAt"]);
    self.ppt_title = SAFESTRING(dictionary[@"coursewareTitle"]);

}
@end
@implementation PPTItemModel

-(void)parseJsonData:(NSDictionary *)dictionary
{
    self.pptItem_id = SAFESTRING(dictionary[@"id"]);
    self.pptItem_time = SAFESTRING(dictionary[@"updatedAt"]);
    self.pptItem_audioUrl = SAFESTRING(dictionary[@"audioUrl"]);
    self.pptItem_imageUrl = SAFESTRING(dictionary[@"imageUrl"]);
    self.pptItem_itemContent = SAFESTRING(dictionary[@"itemContent"]);
    self.pptItem_coursewareId = SAFESTRING(dictionary[@"coursewareId"]);
}

@end
