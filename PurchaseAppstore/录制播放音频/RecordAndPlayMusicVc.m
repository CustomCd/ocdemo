//
//  RecordAndPlayMusicVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/11.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "RecordAndPlayMusicVc.h"
#import "RecordAndPlayMusicHeader.h"
#import "RecordVoiceCollectionViewCell.h"
#import "PaiAudioPlayerEngine.h"
@interface RecordAndPlayMusicVc ()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic, strong) UITableView *record_tableview;
@property (nonatomic, strong) NSMutableArray *record_arr;
@property (nonatomic, strong) RecordAndPlayMusicHeader *record_header;
@property (nonatomic, strong) UICollectionView *record_collectionview;

@end

@implementation RecordAndPlayMusicVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"PPT 详情";
    [self.view addSubview:self.record_collectionview];
    [self.record_collectionview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(KIsIPhoneX?-34:0);
        
    }];
    [self getPPtDetailRequest];
    [self addKeyboardDismissGesture];
}
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.record_arr.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    RecordVoiceCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"RecordVoiceCollectionViewCell" forIndexPath:indexPath];
    PPTItemModel *pptItem = self.record_arr.count >0 ?self.record_arr[indexPath.row]:nil;
    [cell handleRecordVoiceCollectionViewCellWith:pptItem];
    __weak typeof(self)weakSelf = self;

    cell.clickPlayPPTItemVoiceActionBlock = ^(PPTItemButtonClickStatus kPPTItemButtonClickStatus,NSString *endRecordPath) {
        switch (kPPTItemButtonClickStatus) {
            case kPPTItemButtonPlayVoiceAction:
                [weakSelf playVoiceDataWith:indexPath.row];
                break;
                
            case kPPTItemButtonStopVoiceAction:
                [weakSelf stopPlayingVoice];
                break;
            case kPPTItemButtonEndRecordVoiceAction:
                pptItem.pptItem_localAudioUrl = endRecordPath;
                break;
        }
    };
    cell.clickSaveLocalVoiceButtonAction = ^(NSString * _Nonnull ppt_contentt) {
        [weakSelf updatePPTItemVoiceDataRequestWithIndex:indexPath.row pptContent:ppt_contentt];
    };
    return cell;

}

#pragma mark -----------网络请求
- (void)getPPtDetailRequest
{
    NSString *url = @"/vms_api/app/courseware/courseware_info";
    NSDictionary *parameters = @{
        @"coursewareId":self.ppt_id
    };
    [NetWorkingManager postWithUrl:[PPTHOST stringByAppendingString:url] params:parameters success:^(id json) {
        NSLog(@"ppt ====%@",json);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([json[@"code"] integerValue] > 0 ||
            [json[@"status"] isEqualToString:@"failure"]) {
            if ([json[@"code"] integerValue] != 1008) {
//                [[CustomAlert sharedInstance] alertWith:[NetWorkingManager getErrorStrWithCode:json[@"code"]]];
            }
            return ;
        }
        NSArray *items_arr = SAFENSARRY(json[@"coursewareItemList"]);
        [items_arr enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            PPTItemModel *item = [[PPTItemModel alloc] init];
            [item parseJsonData:obj];
            [self.record_arr addObject:item];
        }];
        [self.record_collectionview reloadData];
    } failure:^(NSDictionary *error) {
        
    }];
}
- (void)updatePPTItemVoiceDataRequestWithIndex:(NSInteger)clickIndex pptContent:(NSString *)pptConetnt
{
    PPTItemModel *pptItem = self.record_arr[clickIndex];
    if (pptItem.pptItem_localAudioUrl.length <= 0) {
        jxt_showToastTitle(@"暂无本地录音",  1);
        return;
    }
    NSData *voiceData = nil;
    NSArray *formData = nil;

    if (pptItem.pptItem_localAudioUrl.length >0) {
        voiceData = [NSData dataWithContentsOfFile:pptItem.pptItem_localAudioUrl];
        NSLog(@"文件长度=====%ld",voiceData.length);
        formData = @[@{@"data":voiceData,@"type":@"mp3"}];
    }
    NSString *url = @"/vms_api/app/courseware/update_courseware_item";
    NSDictionary *parameters = @{
        @"coursewareItemId":pptItem.pptItem_id,
        @"itemContent":pptConetnt
    };
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkingManager postWithUrl:[PPTHOST stringByAppendingString:url] params:parameters formData:formData success:^(id json) {
        NSLog(@"%@",json);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        if ([json[@"code"] integerValue] > 0 ||
            [json[@"status"] isEqualToString:@"failure"]) {
            if ([json[@"code"] integerValue] != 1008) {
//                [[CustomAlert sharedInstance] alertWith:[NetWorkingManager getErrorStrWithCode:json[@"code"]]];
            }
            return ;
        }
        jxt_showToastTitle(@"保存成功", 1);
    } failure:^(NSDictionary *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }];
}
#pragma mark -----------播放音频
- (void)playVoiceDataWith:(NSInteger )clickIndex
{
    PPTItemModel *pptItem = self.record_arr[clickIndex];
    
    if (pptItem.pptItem_audioUrl.length <= 0 &&pptItem.pptItem_localAudioUrl.length <=0) {
        jxt_showToastTitle(@"暂无播放资源", 1);
        return;
    }
    
    RecordVoiceCollectionViewCell *cell = (RecordVoiceCollectionViewCell*)[self.record_collectionview cellForItemAtIndexPath:[NSIndexPath indexPathForItem:clickIndex inSection:0]];
    if (pptItem.pptItem_localAudioUrl.length >0) {
        [cell playLocalVoiceWithLocalPath:pptItem.pptItem_localAudioUrl];
//        [[PaiAudioPlayerEngine shareInstance] playMusicWithPath:pptItem.pptItem_localAudioUrl isVideo:NO onView:self.view];
//        [cell startPlayVoiceTimerAndUpdateVoiceDuration:[[PaiAudioPlayerEngine shareInstance] getPlayDurationTime]];
    }else{
        [[PaiAudioPlayerEngine shareInstance] playMusicWithPath:pptItem.pptItem_audioUrl isVideo:NO onView:self.view];
        [cell startPlayVoiceTimerAndUpdateVoiceDuration:[[PaiAudioPlayerEngine shareInstance] getPlayDurationTime]];
    }
}

- (void)stopPlayingVoice
{
    [[PaiAudioPlayerEngine shareInstance] stop];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (NSMutableArray *)record_arr{
    if(!_record_arr){
        _record_arr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
//            NSArray *paths_arr = [[NSFileManager defaultManager] contentsOfDirectoryAtPath:cachesRecorderPath error:nil];
//            for (int i = 0; i<paths_arr.count; i++) {
//                NSString *path = [paths_arr objectAtIndex:i];
//                RecordModel *record = [[RecordModel alloc] init];
//                record.record_path = [cachesRecorderPath stringByAppendingPathComponent:path];
//                record.record_title = path;
//                record.record_num = i;
//                [object addObject:record];
//            }
            
            object;
       });
    }
    return _record_arr;
}


- (RecordAndPlayMusicHeader *)record_header{
    if(!_record_header){
        _record_header = ({
            RecordAndPlayMusicHeader * object = [[RecordAndPlayMusicHeader alloc]initWithFrame:CGRectMake(0, 0, [[UIScreen mainScreen]bounds].size.width, 100)];
            object;
       });
    }
    return _record_header;
}

- (UICollectionView *)record_collectionview{
    if(!_record_collectionview){
        _record_collectionview = ({
            UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
            layout.itemSize = CGSizeMake(kSCREEN_WIDTH, kSCREEN_HEIGHT-64);
            layout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
            layout.minimumLineSpacing = 0;
            UICollectionView * object = [[UICollectionView alloc]initWithFrame:CGRectZero collectionViewLayout:layout];

            object.delegate = self;
            object.dataSource = self;
            object.pagingEnabled = YES;
            [object registerClass:[RecordVoiceCollectionViewCell class] forCellWithReuseIdentifier:@"RecordVoiceCollectionViewCell"];
            object;
       });
    }
    return _record_collectionview;
}
@end
