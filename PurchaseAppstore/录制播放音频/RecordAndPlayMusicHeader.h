//
//  RecordAndPlayMusicHeader.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/11.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecordModel.h"
#import "JKRecorderKit.h"
NS_ASSUME_NONNULL_BEGIN

typedef void(^SaveRecordUpdateListBlock)(RecordModel *record);

@interface RecordAndPlayMusicHeader : UIView
@property (nonatomic, assign) NSInteger record_num;
@property (nonatomic, copy) SaveRecordUpdateListBlock saveRecordUpdateListBlock;

@end

NS_ASSUME_NONNULL_END
