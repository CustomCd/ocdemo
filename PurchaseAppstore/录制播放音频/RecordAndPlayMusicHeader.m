//
//  RecordAndPlayMusicHeader.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/11.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "RecordAndPlayMusicHeader.h"
#import <Masonry/Masonry.h>

@interface RecordAndPlayMusicHeader ()

@property (nonatomic, strong) UIButton *begin_record_button;
@property (nonatomic, strong) UIButton *end_record_button;
@property (nonatomic, strong) UIButton *play_record_button;
@property (nonatomic, strong) UILabel *record_time_label;
@property (nonatomic, strong) UIButton *save_record_button;
@property (nonatomic, strong) NSTimer *record_timer;
@property (nonatomic, assign) double record_time_num;
@property (nonatomic, strong) NSString *record_end_path;



@end
@implementation RecordAndPlayMusicHeader

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addRecordAndPlayMusicHeaderSubviews];
    }
    return self;
}
- (UIButton *)customButtonWithTitle:(NSString *)title
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:title forState:UIControlStateNormal];
    if ([title containsString:@"开始"]||
        [title containsString:@"结束"]) {
        [button setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
    }
    [button setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    button.layer.borderWidth = 1;
    button.layer.cornerRadius = 5;
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    return button;

}
- (void)addRecordAndPlayMusicHeaderSubviews
{
    self.record_num = 0;
    [self addSubview:self.begin_record_button];
    [self addSubview:self.end_record_button];
    [self addSubview:self.play_record_button];
    [self addSubview:self.save_record_button];
    [self addSubview:self.record_time_label];

    [self.record_time_label mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(20);
        make.top.equalTo(self).offset(20);
    }];
    [@[self.begin_record_button,self.end_record_button,self.play_record_button,self.save_record_button] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:20 leadSpacing:20 tailSpacing:20];
    [@[self.begin_record_button,self.end_record_button,self.play_record_button,self.save_record_button] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.record_time_label.mas_bottom).offset(20);
        make.height.mas_equalTo(40);
    }];
}

- (void)buttonAction:(UIButton *)button
{
    button.selected = !button.selected;

    switch (button.tag) {
        case 1:
            [self beginRecordClick];
            self.end_record_button.selected = NO;
            break;
        case 2:
            [self endRecordClick];
            self.begin_record_button.selected = NO;
            break;
        case 3:
            [self playRecordClick];
            break;
        case 4:
            [self saveRecordClick];
            break;
    }
}

#pragma mark 开始录音
-(void)beginRecordClick{
    
    // 不同的文件格式，存放不同的编码数据，caf结尾的文件，基本上可以存放任何苹果支持的编码格式
    NSString *recordNmae = [NSString stringWithFormat:@"record_%ld",self.record_num];
    [[JKAudioTool shareJKAudioTool]beginRecordWithRecordName:recordNmae withRecordType:@"caf" withIsConventToMp3:YES];
    [self beiginRecordTimer];
    
}

#pragma mark 暂停录音
-(void)pauseRecordClick{
    
    NSLog(@"暂停录音");
    [[JKAudioTool shareJKAudioTool]pauseRecord];
}

#pragma mark 结束录音
-(void)endRecordClick{
    
    [[JKAudioTool shareJKAudioTool]endRecord];
    NSLog(@"结束录音=====%@",[[JKAudioTool shareJKAudioTool] recordPath]);
    [self getMusicTimeWithFilepath:[[JKAudioTool shareJKAudioTool] recordPath]];
    [self resetTimer];
    self.record_end_path = [cachesRecorderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"record_%ld.mp3",self.record_num]];
}
#pragma mark 保存录音
- (void)saveRecordClick
{
    if (self.saveRecordUpdateListBlock) {
        RecordModel *record = [[RecordModel alloc] init];
        NSString *path = [cachesRecorderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"record_%ld",self.record_num]];
        record.record_title = @"";
        record.record_path = path;
        record.record_time = @"";
        self.saveRecordUpdateListBlock(record);
    }
}


#pragma mark 删除录音
-(void)deleteRecordClick{
 
    [[JKAudioTool shareJKAudioTool]deleteRecord];
}

#pragma mark 播放录音
-(void)playRecordClick{
    
    
    [[JKAudioPlayerTool shareJKAudioPlayerTool] playAudioWith:self.record_end_path];
    [JKAudioPlayerTool shareJKAudioPlayerTool].span = 0;
    [self playRecordTimer];
}
- (double )getMusicTimeWithFilepath:(NSString *)path
{
    NSLog(@"播放路径===%@",path);
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:path] error:nil];
    double duration = player.duration;
    NSLog(@"音频时长:%f",player.duration);

    return duration;
    
//    AVAudioFormat* format = player.format;
//
//
//    NSLog(@"音频声道数:%u",format.channelCount);
//    NSLog(@"采样频率==%lf",format.sampleRate);////默认为:25600,所以也是按照这个频率来计算的

}

- (NSString *)showRecordTimeWithDuration:(double)duration
{
    NSInteger seconds = (int)duration % 60;
    NSInteger minutes = (int)duration / 60;
    NSInteger hours = (int)duration / 3600;
    NSString *second_str = @"";
    NSString *minute_str = @"";
    NSString *hour_str = @"";
    if (seconds > 9) {
        second_str = [NSString stringWithFormat:@"%ld",seconds];
    }else{
        second_str = [@"0" stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%ld",seconds]];
    }
    if (minutes > 9) {
        minute_str = [NSString stringWithFormat:@"%ld",minutes];
    }else{
        minute_str = [@"0" stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%ld",minutes]];
    }
    return [NSString stringWithFormat:@"%@:%@",minute_str,second_str];
}
#pragma 计时器
- (void)beiginRecordTimer
{
    self.record_time_num = 0;
    self.record_time_label.text = @"00:00";
    self.record_timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordTimerAction) userInfo:nil repeats:YES];
}
- (void)playRecordTimer
{
    self.record_time_num = [self getMusicTimeWithFilepath:self.record_end_path];
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
    self.record_timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(playRecordtimerAction) userInfo:nil repeats:YES];
}
- (void)resetTimer
{
    [self.record_timer invalidate];
    self.record_timer = nil;
}

- (void)playRecordtimerAction
{
    self.record_time_num -= 1;
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
    if (self.record_time_num <= -1) {
        [self.record_timer invalidate];
        self.record_timer = nil;
        self.record_time_label.text = @"00:00";
        
    }
}
- (void)recordTimerAction
{
    self.record_time_num += 1;
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (UIButton *)begin_record_button{
    if(!_begin_record_button){
        _begin_record_button = ({
            UIButton * object = [self customButtonWithTitle:@"开始录音"];
            object.tag = 1;
            object;
       });
    }
    return _begin_record_button;
}

- (UIButton *)end_record_button{
    if(!_end_record_button){
        _end_record_button = ({
            UIButton * object = [self customButtonWithTitle:@"结束录音"];
            object.tag = 2;

            object;
       });
    }
    return _end_record_button;
}

- (UIButton *)play_record_button{
    if(!_play_record_button){
        _play_record_button = ({
            UIButton * object = [self customButtonWithTitle:@"播放录音"];
            object.tag = 3;

            object;
       });
    }
    return _play_record_button;
}

- (UILabel *)record_time_label{
    if(!_record_time_label){
        _record_time_label = ({
            UILabel * object = [[UILabel alloc]init];
            object.textAlignment = NSTextAlignmentCenter;
            object;
       });
    }
    return _record_time_label;
}

- (UIButton *)save_record_button{
    if(!_save_record_button){
        _save_record_button = ({
            UIButton * object = [self customButtonWithTitle:@"保存录音"];
            object.tag = 4;
            object;
       });
    }
    return _save_record_button;
}
@end
