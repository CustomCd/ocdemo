//
//  PPTListVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "PPTListVc.h"
#import "PPTInfoModel.h"
#import "RecordAndPlayMusicVc.h"
#import <UMSocialWechatHandler.h>
#import <UMCShare/WXApi.h>
@interface PPTListVc ()<UITableViewDelegate,UITableViewDataSource,UIDocumentPickerDelegate>
@property (nonatomic, strong) UITableView *pptList_tableview;
@property (nonatomic, strong) NSMutableArray *pptList_arr;
@property (nonatomic, strong) UITextField *input_PPT_title;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) NSData *pptData;
@property (nonatomic, strong) UIView *uploadPPTToSeverView;

@end

@implementation PPTListVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.pptList_tableview];
    [self.pptList_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(KIsIPhoneX?-34:0);

    }];
    self.current_page = 1;
    [self getPPtListRequest];
    [self addMJRefreshHeaderAndFooter];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addPPTAction)];
    [self.view addSubview:self.uploadPPTToSeverView];
    [self.uploadPPTToSeverView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.left.right.equalTo(self.view);
        make.top.equalTo(self.view).offset(100);
    }];
    [self addKeyboardDismissGesture];
}

- (void)addPPTAction
{
    //设置支持的文件类型
//    NSArray *documentTypes = @[@"public.content", @"public.text", @"public.source-code ", @"public.image", @"public.audiovisual-content", @"com.adobe.pdf", @"com.apple.keynote.key", @"com.microsoft.word.doc", @"com.microsoft.excel.xls", @"com.microsoft.powerpoint.ppt"];
    NSArray *documentTypes = @[@"public.content",@"com.microsoft.powerpoint.ppt"];

    //调起文件夹,
    UIDocumentPickerViewController *document = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
    document.delegate = self;
    [self presentViewController:document animated:YES completion:nil];
}
- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray<NSURL *> *)urls
{
    NSLog(@"获取多个文件===%@",urls);
   [self handleUploadPPTToSeverViewHidden:NO];

    NSURL *pathUrl = urls[0];
//    NSString *path = [url_str componentsSeparatedByString:@"///"][1];

    __weak typeof(self)weakSelf = self;
    
    BOOL fileUrlAuthozied = [pathUrl startAccessingSecurityScopedResource];
    if(fileUrlAuthozied){
        NSFileCoordinator * fileCoordinator = [[NSFileCoordinator alloc] init];
        NSError * error;
        [fileCoordinator coordinateReadingItemAtURL:pathUrl options:0 error:&error byAccessor:^(NSURL *newURL) {
            weakSelf.pptData = [NSData dataWithContentsOfURL:newURL];

        }];
        [pathUrl stopAccessingSecurityScopedResource];
    }else{
        //Error handling
        
    }
    
    
}

- (void)clickSaveButtonAction
{
    
    if (self.pptData.length <= 0) {
        jxt_showToastTitle(@"获取本地 PPT 资源失败", 1);
        [self handleUploadPPTToSeverViewHidden:YES];
        return;
    }
    [self uploadPPTToSeverRequestWithPPTData:self.pptData];

}

- (void)handleUploadPPTToSeverViewHidden:(BOOL)isHidden
{
    self.uploadPPTToSeverView.hidden = isHidden;
    self.pptList_tableview.hidden = !self.uploadPPTToSeverView.hidden;
}
#pragma mark ---MJRefrsh
- (void)addMJRefreshHeaderAndFooter

{
    __weak typeof(self)weakSelf = self;
    
    self.pptList_tableview.mj_header = [MJRefreshNormalHeader headerWithRefreshingBlock:^{
        weakSelf.current_page = 1;
        [weakSelf getPPtListRequest];
        
    }];
    
    self.pptList_tableview.mj_footer = [MJRefreshBackNormalFooter footerWithRefreshingBlock:^{
        weakSelf.current_page += 1;
        [weakSelf getPPtListRequest];
    }];
}

#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.pptList_arr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    PPTInfoModel *ppt = self.pptList_arr.count >0 ?self.pptList_arr[indexPath.row]:nil;
    cell.textLabel.text = ppt.ppt_title;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PPTInfoModel *ppt = self.pptList_arr[indexPath.row];
    RecordAndPlayMusicVc *detailVc = [[RecordAndPlayMusicVc alloc] init];
    detailVc.ppt_id = ppt.ppt_id;
    [self.navigationController pushViewController:detailVc animated:YES];
}

#pragma mark -----------网络请求
- (void)getPPtListRequest
{
    if (self.pptList_arr.count >0 && self.current_page == 1) {
        [self.pptList_arr removeAllObjects];
    }
    NSString *url = @"/vms_api/app/courseware/courseware_list";
    [NetWorkingManager postWithUrl:[PPTHOST stringByAppendingString:url] params:nil success:^(id json) {
        NSLog(@"%@",json);
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self.pptList_tableview.mj_header endRefreshing];
        [self.pptList_tableview.mj_footer endRefreshing];
        [self handleTotalPageOfListDataWithTableview:self.pptList_tableview list_data:json];
        if ([json[@"code"] integerValue] > 0 ||
            [json[@"status"] isEqualToString:@"failure"]) {
            if ([json[@"code"] integerValue] != 1008) {
//                [[CustomAlert sharedInstance] alertWith:[NetWorkingManager getErrorStrWithCode:json[@"code"]]];
            }
            return ;
        }
        NSDictionary *dic = SAFENSDICTIONARY(json[@"paging"]);
        NSArray *data_arr = SAFENSARRY([dic objectForKey:@"pagedList"]);
        for (NSDictionary *dic in data_arr) {
            PPTInfoModel *ppt = [[PPTInfoModel alloc] init];
            [ppt parseJsonData:dic];
            NSLog(@"%@===%@",ppt.ppt_title,ppt.ppt_id);
            [self.pptList_arr addObject:ppt];
        }
        [self.pptList_tableview reloadData];
        
    } failure:^(NSDictionary *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];

    }];
}

- (void)uploadPPTToSeverRequestWithPPTData:(NSData *)ppt_data
{
    NSString *url = @"/vms_api/app/courseware/create_courseware";
    NSDictionary *parameters = @{
        @"coursewareTitle":self.input_PPT_title.text
    };

    NSLog(@"%ld",ppt_data.length);
//    return;
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [NetWorkingManager postWithUrl:[PPTHOST stringByAppendingString:url] params:parameters formData:@[@{@"data":ppt_data,@"type":@"pptx"}] success:^(id json) {
        NSLog(@"上传文件====%@",json);
        if ([json[@"code"] integerValue] > 0 ||
            [json[@"status"] isEqualToString:@"failure"]) {
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleUploadPPTToSeverViewHidden:YES];
            if ([json[@"code"] integerValue] != 1008) {
//                [[CustomAlert sharedInstance] alertWith:[NetWorkingManager getErrorStrWithCode:json[@"code"]]];
            }
            return ;
        }
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(30 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            [self handleUploadPPTToSeverViewHidden:YES];
            [self getPPtListRequest];
            jxt_showToastTitle(@"PPT上传成功,等待拆分中..", 2);
        });
    } failure:^(NSDictionary *error) {
        [MBProgressHUD hideHUDForView:self.view animated:YES];
        [self handleUploadPPTToSeverViewHidden:YES];

    }];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)pptList_tableview{
    if(!_pptList_tableview){
        _pptList_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            [self setTableviewConfigWith:object];
            object;
       });
    }
    return _pptList_tableview;
}

- (NSMutableArray *)pptList_arr{
    if(!_pptList_arr){
        _pptList_arr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            object;
       });
    }
    return _pptList_arr;
}

- (UITextField *)input_PPT_title{
    if(!_input_PPT_title){
        _input_PPT_title = ({
            UITextField * object = [[UITextField alloc]init];
            object.placeholder = @"请输入 PPT 标题";
            object.layer.borderWidth = 1;
            object;
       });
    }
    return _input_PPT_title;
}

- (UIButton *)saveButton{
    if(!_saveButton){
        _saveButton = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"保存" forState:UIControlStateNormal];
            [object setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
            [object addTarget:self action:@selector(clickSaveButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _saveButton;
}

- (UIView *)uploadPPTToSeverView{
    if(!_uploadPPTToSeverView){
        _uploadPPTToSeverView = ({
            UIView * object = [[UIView alloc]init];
            object.hidden = YES;
            [object addSubview:self.input_PPT_title];
            [object addSubview:self.saveButton];
                [self.input_PPT_title mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(object).offset(20);
                make.right.equalTo(object).offset(-20);
                make.height.mas_equalTo(40);
                make.top.equalTo(object).offset(100);
            }];
            [self.saveButton mas_makeConstraints:^(MASConstraintMaker *make) {
                make.size.mas_equalTo(CGSizeMake(80, 40));
                make.centerX.equalTo(object.mas_centerX);
                make.top.equalTo(self.input_PPT_title.mas_bottom).offset(10);
            }];
            object;
       });
    }
    return _uploadPPTToSeverView;
}
@end
