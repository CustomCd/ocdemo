//
//  RecordVoiceCollectionViewCell.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/12.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "RecordVoiceCollectionViewCell.h"
#import <Masonry/Masonry.h>
#define kRecordPathPrefix @"yimiaotong_"
@interface RecordVoiceCollectionViewCell ()
@property (nonatomic, strong) UIImageView *ppt_img;
@property (nonatomic, strong) UIButton *record_button;
@property (nonatomic, strong) UIButton *playButton;
@property (nonatomic, strong) UILabel *record_time_label;
@property (nonatomic, strong) UIView *recordTool_bottom;
@property (nonatomic, strong) NSTimer *record_timer;
@property (nonatomic, assign) double record_time_num;
@property (nonatomic, strong) NSString *record_path;
@property (nonatomic, assign) NSInteger record_num;
@property (nonatomic, strong) UILabel *isHasPPTRecordLabel;
@property (nonatomic, strong) UIButton *saveButton;
@property (nonatomic, strong) UITextView *ppt_content;
@property (nonatomic, strong) PPTItemModel *cell_pptItemModel;


@end
@implementation RecordVoiceCollectionViewCell
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addRecordVoiceCollectionViewCellSubviews];
    }
    return self;
}
- (void)addRecordVoiceCollectionViewCellSubviews
{
    
    [self addSubview:self.ppt_img];
    [self addSubview:self.recordTool_bottom];
    [self addSubview:self.ppt_content];
    [self.ppt_content mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(70);
        make.top.equalTo(self);
    }];
    [self.ppt_img mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self);
        make.top.equalTo(self.ppt_content.mas_bottom).offset(10);
        make.size.mas_equalTo(CGSizeMake([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.width));
    }];
    [self.recordTool_bottom mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.height.mas_equalTo(100);
        make.bottom.equalTo(self).offset(-30);
    }];

}

#pragma mark --------buttonAction
- (void)playButtonClickAction:(UIButton *)button
{
    if (self.cell_pptItemModel.pptItem_audioUrl.length <= 0 &&self.cell_pptItemModel.pptItem_localAudioUrl.length <=0) {
        if (self.clickPlayPPTItemVoiceActionBlock) {
            self.clickPlayPPTItemVoiceActionBlock(kPPTItemButtonPlayVoiceAction,@"");
        }
        return;
    }
    button.selected = !button.selected;
    if (button.selected) {
        [self playRecordClick];
    }else{
        [self stopPlayRecordClick];
    }
    [self handleToolButtonsWithClickButton:button];
}
- (void)recordButtonClickAction:(UIButton *)button
{
    button.selected = !button.selected;
    if (button.selected) {
        [self beginRecordClick];
    }else{
        [self endRecordClick];
    }
    [self handleToolButtonsWithClickButton:button];
}

- (void)saveButtonClickAction:(UIButton *)button
{
    if (self.ppt_content.text.length <= 0) {
        
        return;
    }
    if (self.clickSaveLocalVoiceButtonAction) {
        self.clickSaveLocalVoiceButtonAction(self.ppt_content.text);
    }
    
}
- (void)handleToolButtonsWithClickButton:(UIButton *)button
{
    switch (button.tag) {
        case 0:
            self.saveButton.enabled = self.playButton.enabled = !button.selected;
            break;
        case 1:
            self.record_button.enabled  = self.saveButton.enabled = !button.selected;
            break;
        case 2:
            break;
    }
}

#pragma mark 录音相关功能
//开始录音
-(void)beginRecordClick{
    
    // 不同的文件格式，存放不同的编码数据，caf结尾的文件，基本上可以存放任何苹果支持的编码格式
    NSString *recordNmae = [NSString stringWithFormat:@"%@%ld",kRecordPathPrefix,(long)self.record_num];
    [[JKAudioTool shareJKAudioTool]beginRecordWithRecordName:recordNmae withRecordType:@"caf" withIsConventToMp3:YES];
    [self startRecordTimer];
    
}

//暂停录音
-(void)pauseRecordClick{
    
    NSLog(@"暂停录音");
    [[JKAudioTool shareJKAudioTool]pauseRecord];
}

//结束录音
-(void)endRecordClick{
    
    [[JKAudioTool shareJKAudioTool]endRecord];
    NSLog(@"结束录音=====%@",[[JKAudioTool shareJKAudioTool] recordPath]);
    [self resetTimer];
    //录制结束后录音地址后缀是mp3 前面已经修改为mp3了,不再是 caf
    NSString *recordPath = [cachesRecorderPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@%ld.mp3",kRecordPathPrefix,(long)self.record_num]];
    if (self.clickPlayPPTItemVoiceActionBlock) {
        self.clickPlayPPTItemVoiceActionBlock(kPPTItemButtonEndRecordVoiceAction, recordPath);
    }
    self.isHasPPTRecordLabel.text = recordPath;
}


//删除录音
-(void)deleteRecordClick{
 
    [[JKAudioTool shareJKAudioTool]deleteRecord];
}

//播放录音
-(void)playRecordClick{
    
    if (self.clickPlayPPTItemVoiceActionBlock) {
        self.clickPlayPPTItemVoiceActionBlock(kPPTItemButtonPlayVoiceAction,@"");
    }
}
//停止播放录音
- (void)stopPlayRecordClick
{
    if (self.clickPlayPPTItemVoiceActionBlock) {
        self.clickPlayPPTItemVoiceActionBlock(kPPTItemButtonStopVoiceAction,@"");
    }
    [self resetTimer];
}
- (double )getVoiceDurationWithFilepath:(NSString *)path
{
    NSLog(@"播放路径===%@",path);
    AVAudioPlayer* player = [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL URLWithString:path] error:nil];
    double duration = player.duration;
    NSLog(@"音频时长:%f",player.duration);
    return duration;
}

- (NSString *)showRecordTimeWithDuration:(double)duration
{
    NSInteger seconds = (int)duration % 60;
    NSInteger minutes = (int)duration / 60;
    NSInteger hours = (int)duration / 3600;
    NSString *second_str = @"";
    NSString *minute_str = @"";
    NSString *hour_str = @"";
    if (seconds > 9) {
        second_str = [NSString stringWithFormat:@"%ld",(long)seconds];
    }else{
        second_str = [@"0" stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%ld",(long)seconds]];
    }
    if (minutes > 9) {
        minute_str = [NSString stringWithFormat:@"%ld",(long)minutes];
    }else{
        minute_str = [@"0" stringByAppendingFormat:@"%@", [NSString stringWithFormat:@"%ld",(long)minutes]];
    }
    return [NSString stringWithFormat:@"%@:%@",minute_str,second_str];
}
#pragma 计时器
- (void)startRecordTimer
{
    [self resetTimer];
    self.record_time_num = 0;
    self.record_time_label.text = @"00:00";
    self.record_timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(recordTimerAction) userInfo:nil repeats:YES];
}
- (void)startPlayVoiceTimerAndUpdateVoiceDuration:(double)duration
{
    [self resetTimer];
    self.record_time_num = duration;
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
    self.record_timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(playRecordtimerAction) userInfo:nil repeats:YES];
}
- (void)resetTimer
{
    [self.record_timer invalidate];
    self.record_timer = nil;
    self.record_time_label.text = @"00:00";
}

- (void)playRecordtimerAction
{
    self.record_time_num -= 1;
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
    if (self.record_time_num <= 0) {
        [self.record_timer invalidate];
        self.record_timer = nil;
        self.record_time_label.text = @"00:00";
        self.playButton.selected = NO;
        [self handleToolButtonsWithClickButton:self.playButton];
    }
}
- (void)recordTimerAction
{
    self.record_time_num += 1;
    NSString *timestr = [self showRecordTimeWithDuration:self.record_time_num];
    self.record_time_label.text = timestr;
}

#pragma mark ----------public
- (void)handleRecordVoiceCollectionViewCellWith:(PPTItemModel *)itemPPt
{
    self.isHasPPTRecordLabel.text = itemPPt.pptItem_audioUrl.length >0 ?itemPPt.pptItem_audioUrl:@"暂无";
    self.record_num = [itemPPt.pptItem_id integerValue];
//    self.record_time_num = [self getMusicTimeWithFilepath:record.record_path];
    if (self.record_time_num >0) {
        self.record_time_label.text = [self showRecordTimeWithDuration:self.record_time_num];
    }
    self.ppt_content.text = itemPPt.pptItem_itemContent;
    [self.ppt_img sd_setImageWithURL:[NSURL URLWithString:itemPPt.pptItem_imageUrl] placeholderImage:nil options:SDWebImageScaleDownLargeImages];//options 解决大图加载内存暴涨闪退问题
}
- (void)playLocalVoiceWithLocalPath:(NSString *)path
{
    [[JKAudioPlayerTool shareJKAudioPlayerTool] playAudioWith:path];
    ;
    [self startPlayVoiceTimerAndUpdateVoiceDuration:[self getVoiceDurationWithFilepath:path]];
}

#pragma mark ----------------setter
- (UIImageView *)ppt_img{
    if(!_ppt_img){
        _ppt_img = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.contentMode = UIViewContentModeScaleAspectFit;
            object.backgroundColor = [UIColor lightGrayColor];
            object;
       });
    }
    return _ppt_img;
}

- (UIButton *)record_button{
    if(!_record_button){
        _record_button = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"开始录音" forState:UIControlStateNormal];
            [object setTitle:@"结束录音" forState:UIControlStateSelected];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
            [object setTitleColor:[UIColor orangeColor] forState:UIControlStateDisabled];
            object.titleLabel.font = [UIFont systemFontOfSize:15];
            [object addTarget:self action:@selector(recordButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 0;
            object;
       });
    }
    return _record_button;
}

- (UIButton *)playButton{
    if(!_playButton){
        _playButton = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"开始播放" forState:UIControlStateNormal];
            [object setTitle:@"结束播放" forState:UIControlStateSelected];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor redColor] forState:UIControlStateSelected];
            [object setTitleColor:[UIColor orangeColor] forState:UIControlStateDisabled];
            object.titleLabel.font = [UIFont systemFontOfSize:15];
            [object addTarget:self action:@selector(playButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 1;
            object;
       });
    }
    return _playButton;
}

- (UILabel *)record_time_label{
    if(!_record_time_label){
        _record_time_label = ({
            UILabel * object = [[UILabel alloc]init];
            object.textAlignment = NSTextAlignmentCenter;
            object.font = [UIFont systemFontOfSize:15];
            object;
       });
    }
    return _record_time_label;
}


- (UIView *)recordTool_bottom{
    if(!_recordTool_bottom){
        _recordTool_bottom = ({
            UIView * object = [[UIView alloc]init];
            object.backgroundColor = [UIColor lightGrayColor];
            [object addSubview:self.isHasPPTRecordLabel];
            [object addSubview:self.record_button];
            [object addSubview:self.playButton];
            [object addSubview:self.saveButton];
            [object addSubview:self.record_time_label];
            [self.record_time_label mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.equalTo(object);
                make.top.equalTo(object);
                make.height.mas_equalTo(20);
            }];
            [self.isHasPPTRecordLabel mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.right.bottom.equalTo(object);
                make.height.mas_equalTo(20);
            }];
            [@[self.record_button,self.playButton,self.saveButton] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:5 leadSpacing:20 tailSpacing:20];
            [@[self.record_button,self.playButton,self.saveButton] mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(self.record_time_label.mas_bottom);
                make.height.mas_equalTo(40);
            }];
            object;
       });
    }
    return _recordTool_bottom;
}

- (UILabel *)isHasPPTRecordLabel{
    if(!_isHasPPTRecordLabel){
        _isHasPPTRecordLabel = ({
            UILabel * object = [[UILabel alloc]init];
            object.numberOfLines = 2;
            object.font = [UIFont systemFontOfSize:14];
            object;
       });
    }
    return _isHasPPTRecordLabel;
}

- (UIButton *)saveButton{
    if(!_saveButton){
        _saveButton = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"保存录音" forState: UIControlStateNormal];
            [object setTitleColor:[UIColor orangeColor] forState:UIControlStateDisabled];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            object.titleLabel.font = [UIFont systemFontOfSize:15];
            [object addTarget:self action:@selector(saveButtonClickAction:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 2;
            object;
       });
    }
    return _saveButton;
}

- (UITextView *)ppt_content{
    if(!_ppt_content){
        _ppt_content = ({
            UITextView * object = [[UITextView alloc]init];
            object.layer.borderWidth = 1;
            object.layer.borderColor = [UIColor lightGrayColor].CGColor;
            object;
       });
    }
    return _ppt_content;
}
@end
