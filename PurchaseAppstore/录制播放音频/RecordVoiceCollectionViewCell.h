//
//  RecordVoiceCollectionViewCell.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/12.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JKRecorderKit.h"
#import "PPTInfoModel.h"
#import <SDWebImage/SDWebImage.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger,PPTItemButtonClickStatus)  {
    kPPTItemButtonPlayVoiceAction,
    kPPTItemButtonStopVoiceAction,
    kPPTItemButtonEndRecordVoiceAction
};

typedef void(^ClickSaveLocalVoiceButtonAction)(NSString *ppt_contentt);
typedef void(^ClickPlayPPTItemVoiceActionBlock)(PPTItemButtonClickStatus kPPTItemButtonClickStatus,NSString *endRecordPath);

@interface RecordVoiceCollectionViewCell : UICollectionViewCell
@property (nonatomic, copy) ClickPlayPPTItemVoiceActionBlock clickPlayPPTItemVoiceActionBlock;
@property (nonatomic, copy) ClickSaveLocalVoiceButtonAction clickSaveLocalVoiceButtonAction;

- (void)handleRecordVoiceCollectionViewCellWith:(PPTItemModel *)itemPPt;
- (void)startPlayVoiceTimerAndUpdateVoiceDuration:(double)duration;
- (void)playLocalVoiceWithLocalPath:(NSString *)path;
@end

NS_ASSUME_NONNULL_END
