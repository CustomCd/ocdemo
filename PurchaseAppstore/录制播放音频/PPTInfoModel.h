//
//  PPTInfoModel.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseDataModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface PPTInfoModel : BaseDataModel
@property (nonatomic, copy) NSString *ppt_id;
@property (nonatomic, copy) NSString *ppt_url;
@property (nonatomic, copy) NSString *ppt_title;
@property (nonatomic, copy) NSString *ppt_time;


@end


@interface PPTItemModel : BaseDataModel
@property (nonatomic, copy) NSString *pptItem_id;
@property (nonatomic, copy) NSString *pptItem_audioUrl;
@property (nonatomic, copy) NSString *pptItem_itemContent;
@property (nonatomic, copy) NSString *pptItem_coursewareId;
@property (nonatomic, copy) NSString *pptItem_time;
@property (nonatomic, copy) NSString *pptItem_imageUrl;
@property (nonatomic, copy) NSString *pptItem_localAudioUrl;

@end
NS_ASSUME_NONNULL_END
