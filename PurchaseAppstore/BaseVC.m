//
//  BaseVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "BaseVC.h"
@interface BaseVC ()<UIGestureRecognizerDelegate>

@end

@implementation BaseVC
- (void)dealloc
{
    NSLog(@"%@", [NSString stringWithFormat:@"%@ 释放了",NSStringFromClass([self class])]);
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
}
- (void)handleTotalPageOfListDataWithTableview:(UITableView *)tableview list_data:(id)list_data
{
    if (list_data == nil || !list_data) {
        return;
    }
    NSDictionary *data_dic = SAFENSDICTIONARY(list_data[@"paging"]);
    if ([[data_dic allKeys] count] <= 0) {
        data_dic = SAFENSDICTIONARY(list_data[@"data"]);
    }
    if (data_dic) {
        NSString *total_page = SAFESTRING([data_dic objectForKey:@"totalPage"]);
        if (total_page.length <= 0) {
            total_page = SAFESTRING([data_dic objectForKey:@"pages"]);
        }
        if ([total_page integerValue] <= self.current_page) {
            [tableview.mj_footer endRefreshingWithNoMoreData];
        }
    }
}

//tableview 基本设置
- (void)setTableviewConfigWith:(UITableView *)tableview
{
    tableview.delegate = self;
    tableview.dataSource = self;
    tableview.separatorStyle = UITableViewCellSelectionStyleNone;
//    tableview.backgroundColor = kBackgroudColor;
    tableview.backgroundColor = [UIColor whiteColor];
    tableview.showsVerticalScrollIndicator = NO;

    //解决MJ上拉刷新是Cell 上滑的问题
    tableview.estimatedRowHeight = 0;
    tableview.estimatedSectionFooterHeight = 0;
    tableview.estimatedSectionHeaderHeight = 0;
}

- (void)addKeyboardDismissGesture
{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(keyboardDismissAction)];
    tap.delegate = self;
    [self.view addGestureRecognizer:tap];
}

- (void)keyboardDismissAction
{
    [[[UIApplication sharedApplication] keyWindow] endEditing:YES];
}
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    if ([NSStringFromClass([touch.view class]) isEqualToString:@"UITableViewCellContentView"]
        ) {
        return NO;
    }
    return YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
