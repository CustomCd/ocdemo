//
//  GaoDeVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "GaoDeVC.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <MAMapKit/MAMapKit.h>
#import <MapKit/MKTypes.h>
#import "LocationManager.h"
#import "SearchLocationVC.h"
#import "SignInVC.h"
#import <AMapSearchKit/AMapSearchKit.h>
#import "MapModel.h"

@interface GaoDeVC ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate>
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, assign) BOOL removeMapView;
@property (nonatomic, strong) UITableView *map_tableview;
@property (nonatomic, strong) NSMutableArray *locations;
@property (nonatomic, strong) UIImageView          *centerAnnotationView;


@end

@implementation GaoDeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    if (!_mapView) {
        //高德地图
        [self initializationGaoDeMap];
    }
    [self.view addSubview:self.map_tableview];
    [self.map_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(self.mapView.bounds.size.height);
    }];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 40, 40);
    [rightBtn setTitle:@"签到" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [rightBtn addTarget:self action:@selector(showSearchLocationVc) forControlEvents:UIControlEventTouchUpInside];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enterSignInVc) name:@"enterSignInVc" object:nil];
//    [self getCurrentLocation];
}

- (void)showSearchLocationVc
{
    SearchLocationVC *searchVc= [[SearchLocationVC alloc] init];
    
    [self.navigationController presentViewController:[[UINavigationController alloc]initWithRootViewController:searchVc]  animated:YES completion:nil];
    
}

- (void)enterSignInVc
{

    [self.locations removeAllObjects];
    [self.locations addObjectsFromArray: [LocationManager getLocations]];
    [self.map_tableview reloadData];
    
}


#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.locations.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:indentifier];
    }
    MapModel *map = self.locations.count >0?self.locations[indexPath.row]:nil;
    
    cell.textLabel.text = [NSString stringWithFormat:@"%@,%@",map.longitude,map.latitude];
    cell.detailTextLabel.text = map.address;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    MapModel *map = self.locations[indexPath.row];
    [self didTableViewSelectedChanged:CLLocationCoordinate2DMake([map.latitude doubleValue], [map.longitude doubleValue])];
    SignInVC *signIn = [[SignInVC alloc] init];
    signIn.mapModel = map;
    [self.navigationController pushViewController:signIn animated:NO];
    
}
- (void)didTableViewSelectedChanged:(CLLocationCoordinate2D )coordinate
{
    
    CLLocationCoordinate2D location = coordinate;
    
    [self.mapView setCenterCoordinate:location animated:YES];
}

#pragma mark -------地图相关
static const CGFloat kZoomLevel = 17;

- (void)initializationGaoDeMap
{
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height =[[UIScreen mainScreen] bounds].size.height;
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, width,height/2)];
    _mapView.showsCompass = NO;
    _mapView.showsScale = NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    self.mapView.zoomLevel = kZoomLevel;
    _mapView.rotateCameraEnabled = NO;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    
    [self.view addSubview:_mapView];
    [self.mapView addSubview:self.centerAnnotationView];



}
#pragma mark - MapViewDelegate

- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
//    if (!self.isMapViewRegionChangedFromTableView && self.mapView.userTrackingMode == MAUserTrackingModeNone)
//    {
//        [self actionSearchAroundAt:self.mapView.centerCoordinate];
//    }
//    self.isMapViewRegionChangedFromTableView = NO;
}





- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)map_tableview{
    if(!_map_tableview){
        _map_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object.tableFooterView = [[UIView alloc] init];

            object;
       });
    }
    return _map_tableview;
}

- (NSMutableArray *)locations{
    if(!_locations){
        _locations = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            [object addObjectsFromArray: [LocationManager getLocations]];
            object;
       });
    }
    return _locations;
}
- (UIImageView *)centerAnnotationView{
    if(!_centerAnnotationView){
        _centerAnnotationView = ({
            UIImageView * object = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wateRedBlank"]];
            object.center = CGPointMake(self.mapView.center.x, self.mapView.center.y - CGRectGetHeight(object.bounds) / 2);
            object;
        });
    }
    return _centerAnnotationView;
}




@end
