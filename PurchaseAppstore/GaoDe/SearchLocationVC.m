//
//  SearchLocationVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/22.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "SearchLocationVC.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <MAMapKit/MAMapKit.h>
#import <MapKit/MKTypes.h>
#import <AMapSearchKit/AMapSearchKit.h>

#import "MapModel.h"
#import "LocationManager.h"
@interface SearchLocationVC ()<UITableViewDelegate,UITableViewDataSource,MAMapViewDelegate,AMapSearchDelegate,UITextFieldDelegate>
{
    BOOL show_search;
}
@property (nonatomic, strong) AMapLocationManager *locationManager;
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, assign) BOOL removeMapView;
@property (nonatomic, strong) UITableView *map_tableview;
@property (nonatomic, assign) BOOL                  isMapViewRegionChangedFromTableView;

@property (nonatomic, assign) BOOL                  isLocated;
@property (nonatomic, assign) NSInteger             searchPage;
@property (nonatomic, strong) AMapSearchAPI        *search;

@property (nonatomic, strong) NSMutableArray *searchPoiArray;

@property (nonatomic, strong) AMapPOI *selectedPoi;

@property (nonatomic, strong) NSIndexPath *selectedIndexPath;
@property (nonatomic, copy) NSString *currentAddress;
@property (nonatomic, copy) NSString *currentCity;
@property (nonatomic, assign) CLLocationCoordinate2D currentCoordinate;


@property (nonatomic, strong) UIImageView          *centerAnnotationView;
@property (nonatomic, strong) UITextField *search_textifled;
@property (nonatomic, strong) UITableView *search_tableview;
@property (nonatomic, strong) NSMutableArray *search_arr;


@end

@implementation SearchLocationVC


- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"enterSignInVc" object:nil];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"定位";
    show_search = NO;
    [self.view addSubview:self.search_textifled];
    [self.search_textifled mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(15);
        make.right.equalTo(self.view).offset(-15);
        make.top.equalTo(self.view).offset(64);
        make.height.mas_equalTo(40);
    }];

    
    if (!_mapView) {
        //高德地图
        [self initializationGaoDeMap];
    }
    [self.view addSubview:self.map_tableview];
    [self.map_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view).offset(self.mapView.bounds.size.height+self.mapView.frame.origin.y);
    }];
    
    UIButton *cancle = [UIButton buttonWithType:UIButtonTypeCustom];
    cancle.frame = CGRectMake(0, 0, 40, 40);
    [cancle setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [cancle setTitle:@"取消" forState:UIControlStateNormal];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:cancle];
    
    UIButton *submit = [UIButton buttonWithType:UIButtonTypeCustom];
    submit.frame = CGRectMake(0, 0, 40, 40);
    [submit setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    [submit setTitle:@"提交" forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:submit];
    [cancle addTarget:self action:@selector(clickCancelBtnAction) forControlEvents:UIControlEventTouchUpInside];
    [submit addTarget:self action:@selector(clickSubmitBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [self.view addSubview:self.search_tableview];
    [self.search_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.search_textifled.mas_bottom);
        make.bottom.equalTo(self.view);
    }];
    self.search_tableview.hidden = !show_search;

}

#pragma mark -----左右 item
- (void)clickCancelBtnAction
{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)clickSubmitBtnAction
{
    MapModel *map = [[MapModel alloc] init];
    if (self.selectedPoi == nil) {
        map.address = self.currentAddress;
        map.longitude = [NSString stringWithFormat:@"%.6f", self.mapView.centerCoordinate.longitude];
        map.latitude = [NSString stringWithFormat:@"%.6f", self.mapView.centerCoordinate.latitude];

    }else{
        [map parseDataWith:self.selectedPoi];

    }
    [LocationManager saveLocation:map];
    [self dismissViewControllerAnimated:YES completion:nil];

}

#pragma mark-----地图配置
static const CGFloat kZoomLevel = 13.1;

- (void)initializationGaoDeMap
{
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height =[[UIScreen mainScreen] bounds].size.height;
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 104, width,height/2)];
    _mapView.showsCompass = NO;
    _mapView.showsScale = NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    self.mapView.zoomLevel = 17;
    _mapView.rotateCameraEnabled = NO;
    self.mapView.showsUserLocation = YES;
//    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    
    [self.view addSubview:_mapView];
    [self.mapView addSubview:self.centerAnnotationView];
    
    self.searchPage = 1;
    self.search = [[AMapSearchAPI alloc] init];
    self.search.delegate = self;
    
    
}



#pragma mark ------tableviewdelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return show_search?1:2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (show_search) {
        return self.search_arr.count;
    }else{
        if (section == 0) {
            return 1;
        }else{
            return self.searchPoiArray.count;
            
        }
    }
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (show_search) {
        static NSString *reusedIndentifier = @"reusedIndentifier";
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusedIndentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reusedIndentifier];
        }
        AMapTip *tip = self.search_arr[indexPath.row];
        cell.textLabel.text = tip.name;
        cell.detailTextLabel.text = tip.address;
        
        return cell;
    }
    static NSString *reusedIndentifier = @"reusedIndentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reusedIndentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:reusedIndentifier];
    }
    if (indexPath.section == 0)
    {
        cell.textLabel.text = @"[位置]";
        cell.detailTextLabel.text = self.currentAddress;
    }
    else
    {
        AMapPOI *poi = self.searchPoiArray[indexPath.row];
        cell.textLabel.text = poi.name;
        cell.detailTextLabel.text = poi.address;
    }
    
    if (self.selectedIndexPath && self.selectedIndexPath.section == indexPath.section && self.selectedIndexPath.row == indexPath.row)
    {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    else
    {
        cell.accessoryType = UITableViewCellAccessoryNone;
    }
    
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (show_search) {
        show_search = NO;
        [self handleSearchTableview];
        AMapTip *tip = self.search_arr[indexPath.row];
        [self actionSearchAroundAt:CLLocationCoordinate2DMake(tip.location.latitude, tip.location.longitude)];
        [self.search_textifled resignFirstResponder];
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(tip.location.latitude, tip.location.longitude) animated:YES];

        return;
    }
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType= UITableViewCellAccessoryCheckmark;
    self.selectedIndexPath = indexPath;
    
    if (indexPath.section == 0)
    {
        self.selectedPoi = nil;
        [self didPositionCellTapped];
        return ;
    }
    self.selectedPoi = self.searchPoiArray[indexPath.row];
    [self didTableViewSelectedChanged:self.selectedPoi];

}

- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryNone;
}

- (void)didTableViewSelectedChanged:(AMapPOI *)selectedPoi
{
    // 防止连续点两次
    if(self.isMapViewRegionChangedFromTableView == YES)
    {
        return;
    }
    
    self.isMapViewRegionChangedFromTableView = YES;
    
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake(selectedPoi.location.latitude, selectedPoi.location.longitude);
    
    [self.mapView setCenterCoordinate:location animated:YES];
}

- (void)didPositionCellTapped
{
    // 防止连续点两次
    if(self.isMapViewRegionChangedFromTableView == YES)
    {
        return;
    }
    
    self.isMapViewRegionChangedFromTableView = YES;
    
    [self.mapView setCenterCoordinate:self.currentCoordinate animated:YES];
}



#pragma mark - MapViewDelegate

- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    if (!self.isMapViewRegionChangedFromTableView && self.mapView.userTrackingMode == MAUserTrackingModeNone)
    {
        [self actionSearchAroundAt:self.mapView.centerCoordinate];
    }
    self.isMapViewRegionChangedFromTableView = NO;
}

- (void)mapView:(MAMapView *)mapView didUpdateUserLocation:(MAUserLocation *)userLocation updatingLocation:(BOOL)updatingLocation
{
    if(!updatingLocation)
        return ;
    
    if (userLocation.location.horizontalAccuracy < 0)
    {
        return ;
    }
    
    // only the first locate used.
    if (!self.isLocated)
    {
        self.isLocated = YES;
        self.mapView.userTrackingMode = MAUserTrackingModeFollow;
        [self.mapView setCenterCoordinate:CLLocationCoordinate2DMake(userLocation.location.coordinate.latitude, userLocation.location.coordinate.longitude)];
        
        [self actionSearchAroundAt:userLocation.location.coordinate];
    }
}

#pragma mark - AMapSearchDelegate

- (void)onPOISearchDone:(AMapPOISearchBaseRequest *)request response:(AMapPOISearchResponse *)response
{
//    if (self.isFromMoreButton == YES)
//    {
//        self.isFromMoreButton = NO;
//    }
//    else
//    {
//        [self.searchPoiArray removeAllObjects];
////        [self.moreButton setTitle:kMoreButtonTitle forState:UIControlStateNormal];
////        self.moreButton.enabled = YES;
////        self.moreButton.backgroundColor = [UIColor whiteColor];
//    }
    [self.searchPoiArray removeAllObjects];


    if (response.pois.count == 0)
    {
//        [self.moreButton setTitle:@"没有数据了.." forState:UIControlStateNormal];
//        self.moreButton.enabled = NO;
//        self.moreButton.backgroundColor = [[UIColor grayColor] colorWithAlphaComponent:0.4];
//        [self.tableView reloadData];

        self.selectedIndexPath = nil;
        return;
    }

    [response.pois enumerateObjectsUsingBlock:^(AMapPOI *obj, NSUInteger idx, BOOL *stop) {
        [self.searchPoiArray addObject:obj];
    }];

    self.selectedIndexPath = nil;
    [self.map_tableview reloadData];
}

- (void)onReGeocodeSearchDone:(AMapReGeocodeSearchRequest *)request response:(AMapReGeocodeSearchResponse *)response
{
    if (response.regeocode != nil)
    {
        self.currentAddress = response.regeocode.formattedAddress;
        self.currentCity = response.regeocode.addressComponent.city;
//        for (int i = 0; i<response.regeocode.pois.count; i++) {
//            AMapPOI *poi = response.regeocode.pois[i];
//            if (poi.location.latitude == self.mapView.userLocation.coordinate.latitude &&
//                poi.location.longitude == self.mapView.userLocation.coordinate.longitude) {
//                NSLog(@"%@",poi.name);
//
//            }
//        }
//        NSIndexPath *reloadIndexPath = [NSIndexPath indexPathForRow:0 inSection:0];
//        [self.map_tableview reloadRowsAtIndexPaths:@[reloadIndexPath] withRowAnimation:UITableViewRowAnimationNone];
    }
}


- (void)onInputTipsSearchDone:(AMapInputTipsSearchRequest *)request response:(AMapInputTipsSearchResponse *)response
{
    [self.search_arr removeAllObjects];
    for (AMapTip *tip in response.tips) {
        NSLog(@"%@=%@",tip.name,tip.location);
        [self.search_arr addObject:tip];
    }
    [self.search_tableview reloadData];
}




#pragma mark - Utility

/* 根据中心点坐标来搜周边的POI. */
- (void)searchPoiWithCenterCoordinate:(CLLocationCoordinate2D )coord
{
    AMapPOIAroundSearchRequest*request = [[AMapPOIAroundSearchRequest alloc] init];
    
    request.location = [AMapGeoPoint locationWithLatitude:coord.latitude  longitude:coord.longitude];
    
    request.radius   = 1000;
    //住宅区：120300 学校：141200 楼宇：120200 商场：060111
    request.types = @"住宅|学校|楼宇|商场";
    request.sortrule = 0;//排序顺序
    request.page     = self.searchPage;
    
    [self.search AMapPOIAroundSearch:request];
}

- (void)searchReGeocodeWithCoordinate:(CLLocationCoordinate2D)coordinate
{
    self.currentCoordinate = coordinate;

    AMapReGeocodeSearchRequest *regeo = [[AMapReGeocodeSearchRequest alloc] init];
    
    regeo.location = [AMapGeoPoint locationWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    regeo.requireExtension = YES;
    
    [self.search AMapReGoecodeSearch:regeo];
}


- (void)actionSearchAroundAt:(CLLocationCoordinate2D)coordinate
{
    [self searchReGeocodeWithCoordinate:coordinate];
    [self searchPoiWithCenterCoordinate:coordinate];
    
    self.searchPage = 1;
    [self centerAnnotationAnimimate];
}

- (void)actionLocation
{
    if (self.mapView.userTrackingMode == MAUserTrackingModeFollow)
    {
        [self.mapView setUserTrackingMode:MAUserTrackingModeNone animated:YES];
    }
    else
    {
        self.searchPage = 1;
        
        [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate animated:YES];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            // 因为下面这句的动画有bug，所以要延迟0.5s执行，动画由上一句产生
            [self.mapView setUserTrackingMode:MAUserTrackingModeFollow animated:YES];
        });
    }
}


/* 移动窗口弹一下的动画 */
- (void)centerAnnotationAnimimate
{
    [UIView animateWithDuration:0.5
                          delay:0
                        options:UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         CGPoint center = self.centerAnnotationView.center;
                         center.y -= 20;
                         [self.centerAnnotationView setCenter:center];}
                     completion:nil];
    
    [UIView animateWithDuration:0.45
                          delay:0
                        options:UIViewAnimationOptionCurveEaseIn
                     animations:^{
                         CGPoint center = self.centerAnnotationView.center;
                         center.y += 20;
                         [self.centerAnnotationView setCenter:center];}
                     completion:nil];
}

#pragma mark -----textfield delegate
- (void)textFieldChanged:(UITextField *)textField
{
    show_search = YES;
    [self handleSearchTableview];

    AMapInputTipsSearchRequest*request = [[AMapInputTipsSearchRequest alloc] init];
    
    //住宅区：120300 学校：141200 楼宇：120200 商场：060111
    request.types = @"楼宇";
    //    request.page     = self.searchPage;
    request.keywords = textField.text;
    request.city = self.currentCity;
    
    [self.search AMapInputTipsSearch:request];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

- (void)handleSearchTableview
{
    self.search_tableview.hidden = !show_search;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark -----懒加载
- (UITableView *)map_tableview{
    if(!_map_tableview){
        _map_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object.tableFooterView = [[UIView alloc] init];
            
            object;
        });
    }
    return _map_tableview;
}

- (NSMutableArray *)searchPoiArray{
    if(!_searchPoiArray){
        _searchPoiArray = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            object;
       });
    }
    return _searchPoiArray;
}
- (UIImageView *)centerAnnotationView{
    if(!_centerAnnotationView){
        _centerAnnotationView = ({
            UIImageView * object = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wateRedBlank"]];
            
            object.center = CGPointMake(self.mapView.center.x, self.mapView.frame.size.height/2 - CGRectGetHeight(object.bounds) / 2);
            object;
        });
    }
    return _centerAnnotationView;
}



- (UITextField *)search_textifled{
    if(!_search_textifled){
        _search_textifled = ({
            UITextField * object = [[UITextField alloc]init];
            object.placeholder = @"请输入地址";
            object.delegate = self;
            object.enablesReturnKeyAutomatically = YES;
            [object addTarget:self action:@selector(textFieldChanged:) forControlEvents:UIControlEventEditingChanged];
            object;
       });
    }
    return _search_textifled;
}

- (UITableView *)search_tableview{
    if(!_search_tableview){
        _search_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object.rowHeight = 40;
            object;
       });
    }
    return _search_tableview;
}

- (NSMutableArray *)search_arr{
    if(!_search_arr){
        _search_arr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            object;
       });
    }
    return _search_arr;
}
@end
