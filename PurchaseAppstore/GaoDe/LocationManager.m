//
//  LocationManager.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/22.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "LocationManager.h"

@implementation LocationManager
+ (void)saveLocation:(MapModel *)location
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"locations"];
    
    NSMutableArray *locations = [NSMutableArray array];
    NSArray *arr = [NSKeyedUnarchiver unarchiveObjectWithData:data];
    if (arr.count >0) {
        [locations addObjectsFromArray:arr];
    }
    [locations addObject:location];

    [[NSUserDefaults standardUserDefaults] setObject:[NSKeyedArchiver archivedDataWithRootObject:[locations copy]] forKey:@"locations"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}

+ (NSArray *)getLocations
{
    NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:@"locations"];
    NSArray *locations = [NSKeyedUnarchiver unarchiveObjectWithData:data];

    if (locations.count <= 0) {
        return @[];
    }else{
        return locations;

    }
}
@end
