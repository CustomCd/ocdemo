//
//  MapModel.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "MapModel.h"
#import <objc/runtime.h>

@implementation MapModel
- (void)parseDataWith:(AMapPOI*)poi
{
    self.name = poi.name;
    self.address = poi.address;
    self.longitude = [NSString stringWithFormat:@"%.6f", poi.location.longitude];
    self.latitude = [NSString stringWithFormat:@"%.6f", poi.location.latitude];

//    self.coordinate = CLLocationCoordinate2DMake(poi.location.latitude, poi.location.longitude);
}
- (NSMutableArray *)getProperties:(Class)cls{
    unsigned int count;
    objc_property_t *properties = class_copyPropertyList(cls, &count);
    NSMutableArray *mArray = [NSMutableArray array];
    for (int i = 0; i < count; i++) {
        objc_property_t property = properties[i];
        const char *cName = property_getName(property);
        NSString *name = [NSString stringWithCString:cName encoding:NSUTF8StringEncoding];
        [mArray addObject:name];
    }
    
    return mArray.copy;
}

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    
    unsigned int count = 0;
    Ivar *ivars = class_copyIvarList([self class], &count);
    
    for (int i = 0; i<count; i++) {
        Ivar ivar = ivars[i];
        const char *name = ivar_getName(ivar);
        NSString *key = [NSString stringWithUTF8String:name];
        if ([key isEqualToString:@"_task"]) {
            continue;
        }
        id value = [self valueForKey:key];
        [aCoder encodeObject:value forKey:key];
    }
    free(ivars);
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self = [super init]) {
        unsigned int count = 0;
        Ivar *ivars = class_copyIvarList([self class], &count);
        for (int i = 0; i<count; i++) {
            Ivar ivar = ivars[i];
            const char *name = ivar_getName(ivar);
            NSString *key = [NSString stringWithUTF8String:name];
            if ([key isEqualToString:@"_task"]) {
                continue;
            }
            id value = [aDecoder decodeObjectForKey:key];
            [self setValue:value forKey:key];
        }
        free(ivars);
    }
    return self;
}
@end
