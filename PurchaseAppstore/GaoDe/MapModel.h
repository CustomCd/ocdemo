//
//  MapModel.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AMapSearchKit/AMapSearchKit.h>
@interface MapModel : NSObject
@property (nonatomic, copy) NSString *address;
@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) BOOL isMark;
@property (nonatomic, copy) NSString *latitude;//纬度
@property (nonatomic, copy) NSString *longitude;//经度

- (void)parseDataWith:(AMapPOI*)poi;




@end
