//
//  SignInVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/22.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "SignInVC.h"
#import <AMapFoundationKit/AMapFoundationKit.h>
#import <AMapLocationKit/AMapLocationKit.h>
#import <MAMapKit/MAMapKit.h>
#import <MapKit/MKTypes.h>
@interface SignInVC ()<MAMapViewDelegate>
@property (nonatomic, strong) MAMapView *mapView;
@property (nonatomic, strong) UIImageView          *centerAnnotationView;
@property (nonatomic, strong) AMapGeoFenceManager *geoFenceManager;
@property (nonatomic, assign) CLLocationCoordinate2D currentCoordinate;

@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initializationGaoDeMap];

}
#pragma mark -------地图相关
static const CGFloat kZoomLevel = 13;

- (void)initializationGaoDeMap
{
    CGFloat width = [[UIScreen mainScreen] bounds].size.width;
    CGFloat height =[[UIScreen mainScreen] bounds].size.height;
    _mapView = [[MAMapView alloc] initWithFrame:CGRectMake(0, 0, width,height)];
    _mapView.showsCompass = NO;
    _mapView.showsScale = NO;
    _mapView.delegate = self;
    _mapView.rotateEnabled = NO;
    self.mapView.zoomLevel = kZoomLevel;
    _mapView.rotateCameraEnabled = NO;
    self.mapView.showsUserLocation = YES;
    self.mapView.userTrackingMode = MAUserTrackingModeFollow;
    
    [self.view addSubview:_mapView];
    [self.mapView addSubview:self.centerAnnotationView];
    
    CLLocationCoordinate2D currentCoordinate = CLLocationCoordinate2DMake([self.mapModel.latitude doubleValue], [self.mapModel.longitude doubleValue]);
    [self.mapView setCenterCoordinate:currentCoordinate];
    
    
    //构造圆
    MACircle *circle = [MACircle circleWithCenterCoordinate:currentCoordinate radius:1000];
    
    //在地图上添加圆
    [_mapView addOverlay: circle];
    
    UIButton *rightBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    rightBtn.frame = CGRectMake(0, 0, 40, 40);
    [rightBtn setTitle:@"确认签到" forState:UIControlStateNormal];
    [rightBtn setTitleColor:[UIColor blueColor] forState:UIControlStateNormal];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightBtn];
    [rightBtn addTarget:self action:@selector(clickConfirmButton) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)clickConfirmButton
{
    
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([self.mapModel.latitude doubleValue],[self.mapModel.longitude doubleValue]);
    CLLocationCoordinate2D center = self.mapView.centerCoordinate;
    BOOL isContains = MACircleContainsCoordinate(location, center, 1000);
    NSLog(@"%@",isContains?@"范围内":@"范围外");
    if (isContains) {
        jxt_showAlertOneButton(@"签到成功", @"确认签到成功", @"确定", ^(NSInteger buttonIndex) {
            [self.navigationController popViewControllerAnimated:YES];
        });
        
    }else{
        jxt_showAlertTitleMessage(@"签到失败", @"1000米外签到");

    }
}

#pragma mark - MapViewDelegate

- (void)mapView:(MAMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    //    if (!self.isMapViewRegionChangedFromTableView && self.mapView.userTrackingMode == MAUserTrackingModeNone)
    //    {
    //        [self actionSearchAroundAt:self.mapView.centerCoordinate];
    //    }
    //    self.isMapViewRegionChangedFromTableView = NO;
    
    CLLocationCoordinate2D location = CLLocationCoordinate2DMake([self.mapModel.latitude doubleValue],[self.mapModel.longitude doubleValue]);
    CLLocationCoordinate2D center = mapView.centerCoordinate;
    BOOL isContains = MACircleContainsCoordinate(location, center, 1000);
    NSLog(@"%@",isContains?@"范围内":@"范围外");
    
}

- (MAOverlayRenderer *)mapView:(MAMapView *)mapView rendererForOverlay:(id <MAOverlay>)overlay
{
    if ([overlay isKindOfClass:[MACircle class]])
    {
        MACircleRenderer *circleRenderer = [[MACircleRenderer alloc] initWithCircle:overlay];
        
        circleRenderer.lineWidth    = 5.f;
        circleRenderer.strokeColor  = [UIColor colorWithRed:0.6 green:0.6 blue:0.6 alpha:0.8];
        circleRenderer.fillColor    = [UIColor colorWithRed:1.0 green:0.8 blue:0.0 alpha:0.8];
        return circleRenderer;
    }
    return nil;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIImageView *)centerAnnotationView{
    if(!_centerAnnotationView){
        _centerAnnotationView = ({
            UIImageView * object = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"wateRedBlank"]];
            object.center = CGPointMake(self.mapView.center.x, self.mapView.center.y - CGRectGetHeight(object.bounds) / 2);
            object;
        });
    }
    return _centerAnnotationView;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
