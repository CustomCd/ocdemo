//
//  LocationManager.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/22.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapModel.h"
@interface LocationManager : NSObject
+ (void)saveLocation:(MapModel *)location;
+ (NSArray *)getLocations;

@end
