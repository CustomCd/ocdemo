//
//  RegistVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/7/30.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "RegistVC.h"
#import "NetWorkingManager.h"
#import "JXTAlertView.h"
static NSString * const kWChatRegist =  @"";
static NSString * const kSendCapthca =  @"";



@interface RegistVC ()
@property (nonatomic, strong) UITextField *phone_textfield;
@property (nonatomic, strong) UITextField *captcha_textfield;
@property (nonatomic, strong) UITextField *passowrd_textfield;
@property (nonatomic, strong) UIButton *registBtn;



@end

@implementation RegistVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:self.phone_textfield];
    [self.view addSubview:self.captcha_textfield];
    [self.view addSubview:self.passowrd_textfield];
    [self.view addSubview:self.registBtn];
    
}


- (void)clickRegistBtn
{
    if (self.phone_textfield.text.length <= 0||self.captcha_textfield.text.length <= 0||self.passowrd_textfield.text.length <= 0) {
        jxt_showAlertTitle(@"请输入信息");
        return;
    }
    NSDictionary *regist_parameters = @{
                                        @"uid":self.wchat_parameters[@"unionId"],
                                        @"name":self.wchat_parameters[@"name"],
                                        @"gender":self.wchat_parameters[@"gender"],
                                        @"iconurl":self.wchat_parameters[@"iconurl"],
                                        @"type":@"weixin",
                                        @"accountNumber":self.phone_textfield.text,
                                        @"verificationCode":self.captcha_textfield.text,
                                        @"password":self.passowrd_textfield.text
                                        };
    [NetWorkingManager postWithUrl:kWChatRegist params:regist_parameters success:^(id json) {
        NSLog(@"%@",json);
        if ([json[@"code"] integerValue]>0) {
            jxt_showAlertTitle(json[@"status"]);
            return ;
        }else{
            NSString *success_str = [NSString stringWithFormat:@"注册成功,token:%@",json[@"accessToken"]];
            
            jxt_showAlertTitle(success_str);
        }
    } failure:^(NSDictionary *error) {
        
    }];
}
- (void)clickSendCaptcha:(UIButton *)button
{
    button.selected = button.selected;
    
    [NetWorkingManager postWithUrl:kSendCapthca params:@{@"telephoneNumber":self.phone_textfield.text} formData:nil success:^(id json) {
        
    } failure:^(NSDictionary *error) {
        
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITextField *)phone_textfield{
    if(!_phone_textfield){
        _phone_textfield = ({
            UITextField * object = [[UITextField alloc]initWithFrame:CGRectMake(20, 100, 200, 40)];
            object.placeholder = @"请输入手机号";

            object;
       });
    }
    return _phone_textfield;
}

- (UITextField *)captcha_textfield{
    if(!_captcha_textfield){
        _captcha_textfield = ({
            UITextField * object = [[UITextField alloc]initWithFrame:CGRectMake(20, 150, 200, 40)];
            object.placeholder = @"请输入验证码";
            UIButton *captcha_btn = [UIButton buttonWithType:UIButtonTypeCustom];
            captcha_btn.frame = CGRectMake(0, 0, 80, 40);
            [captcha_btn setTitle:@"获取验证码" forState:UIControlStateNormal];
            [captcha_btn setTitle:@"已经发送" forState:UIControlStateSelected];
            captcha_btn.titleLabel.font = [UIFont systemFontOfSize:13.0];

            [captcha_btn setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            object.rightView = captcha_btn;
            object.rightViewMode = UITextFieldViewModeAlways;
            captcha_btn.selected = NO;
            [captcha_btn addTarget:self action:@selector(clickSendCaptcha:) forControlEvents:UIControlEventTouchUpInside];

            object;
       });
    }
    return _captcha_textfield;
}

- (UITextField *)passowrd_textfield{
    if(!_passowrd_textfield){
        _passowrd_textfield = ({
            UITextField * object = [[UITextField alloc]initWithFrame:CGRectMake(20, 200, 250, 40)];
            object.placeholder = @"请输入密码";
            object;
       });
    }
    return _passowrd_textfield;
}

- (UIButton *)registBtn{
    if(!_registBtn){
        _registBtn = ({
            UIButton * object = [[UIButton alloc]initWithFrame:CGRectMake(10, 250, 100, 40)];
            [object setTitle:@"注  册" forState:UIControlStateNormal];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            [object addTarget:self action:@selector(clickRegistBtn) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _registBtn;
}
@end
