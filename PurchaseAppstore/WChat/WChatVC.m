//
//  WChatVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "WChatVC.h"
#import <AVFoundation/AVFoundation.h>
#import "NetWorkingManager.h"
#import "RegistVC.h"
#import <UMShare/UMShare.h>
#import <UMCShare/WXApiObject.h>
#import <UMCShare/WXApi.h>
#import <UShareUI/UShareUI.h>

static NSString * const kCheckWChatRegist =  @"";

@interface WChatVC ()<UMSocialShareMenuViewDelegate>
{
    BOOL isAuthorization;

}
@property (nonatomic, strong) UIButton *wChatBtn;
@property (nonatomic, strong) UIButton *scanBtn;
@property (nonatomic, strong) UIButton *QQBtn;
@property (nonatomic, strong) UIButton *sniaBtn;
@property (nonatomic, strong) UIButton *evokeWchatBtn;


@end

@implementation WChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"微信登录";
    isAuthorization = YES;
    [self.view addSubview:self.wChatBtn];
    [self.view addSubview:self.QQBtn];
    [self.view addSubview:self.sniaBtn];
    [self.view addSubview:self.evokeWchatBtn];
    
    [@[self.wChatBtn,self.QQBtn,self.sniaBtn] mas_distributeViewsAlongAxis:MASAxisTypeHorizontal withFixedSpacing:10 leadSpacing:10 tailSpacing:10];
    [@[self.wChatBtn,self.QQBtn,self.sniaBtn] mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(40);
        make.top.equalTo(self.view).offset(40+64);
    }];
    [self.evokeWchatBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.top.equalTo(self.view).offset(40+64+50);

    }];
    
    [self.view addSubview:self.scanBtn];

    [self.scanBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.wChatBtn.mas_bottom).offset(40);
        make.centerX.equalTo(self.view);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    
    [NetWorkingManager postWithUrl:@"https://yp.dxshuju.com/user/member/faMember" params:@{@"telephone":@"18210218950"} success:^(id json) {
        NSLog(@"=============%@",json);
    } failure:^(NSDictionary *error) {
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
//    [self getAuthWithUserInfoFromWechat];
}

- (void)getAuthWithUserInfoFromUM:(UIButton *)button
{
    UMSocialPlatformType platformType = UMSocialPlatformType_UnKnown;
    switch (button.tag) {
        case 100:
            platformType = UMSocialPlatformType_WechatSession;
            break;
        case 101:
            platformType = UMSocialPlatformType_QQ;

            break;
        case 102:
            platformType = UMSocialPlatformType_Sina;

            break;
    }
    if (platformType == UMSocialPlatformType_UnKnown) {
        return;
    }
    if (isAuthorization) {
        [[UMSocialManager defaultManager] cancelAuthWithPlatform:platformType completion:^(id result, NSError *error) {
            [self getAuthWithUserInfoFromUM:button];
        }];
    }else{
        [[UMSocialManager defaultManager] getUserInfoWithPlatform:platformType currentViewController:nil completion:^(id result, NSError *error) {
            if (error) {
                NSLog(@"%@",[error localizedDescription]);
                jxt_showAlertTitleMessage(@"登录失败", [error localizedDescription]);
                self.scanBtn.hidden = YES;

            } else {
                
                UMSocialUserInfoResponse *resp = result;
                NSMutableDictionary *requestDic = [NSMutableDictionary dictionary];
                [requestDic setValue:resp.originalResponse[@"sex"] forKey:@"gender"];
                [requestDic setValue:resp.iconurl forKey:@"profile_pic_url"];
                [requestDic setValue:resp.openid forKey:@"sp_id"];
                [requestDic setValue:resp.name forKey:@"user_name"];
                
                //            NSLog(@"%@",resp.openid);
                // 授权信息
                //                NSLog(@"Wechat uid: %@", resp.uid);
                //                NSLog(@"Wechat openid: %@", resp.openid);
                //                NSLog(@"Wechat accessToken: %@", resp.accessToken);
                //                NSLog(@"Wechat refreshToken: %@", resp.refreshToken);
                //                NSLog(@"Wechat expiration: %@", resp.expiration);
                //                // 用户信息
                //                NSLog(@"Wechat name: %@", resp.name);
                //                NSLog(@"Wechat iconurl: %@", resp.iconurl);
                //                NSLog(@"Wechat gender: %@", resp.gender);
                // 第三方平台SDK源数据
//                NSLog(@"Wechat originalResponse: %@", resp.originalResponse);


                //            [self http_thirdLoginWithDic:requestDic];
                NSDictionary *show_str =
  @{
    
    @"uid":resp.uid.length>0?resp.uid:@"",
    @"openid":resp.openid.length>0?resp.openid:@"",
    @"refreshToken":resp.refreshToken.length>0?resp.refreshToken:@"",
    @"expiration":resp.expiration,
    @"accessToken":resp.accessToken.length>0?resp.accessToken:@"",
    @"unionId":resp.unionId.length>0?resp.unionId:@"",
    @"usid":resp.usid.length>0?resp.usid:@"",
    @"platformType":[NSString stringWithFormat:@"%ld", resp.platformType],
    @"name":resp.name.length>0?resp.name:@"",
    @"iconurl":resp.iconurl.length>0?resp.iconurl:@"",
    @"unionGender":resp.unionGender.length>0?resp.unionGender:@"",
    @"gender":resp.gender.length>0?resp.gender:@""                                        
                                        };
//                jxt_showAlertTitleMessage(@"登录成功", sho);
//                NSLog(@"Response: %@", show_str);
                [self checkUserIsRegistSuccssRequestWith:show_str];
//                self.scanBtn.hidden = NO;

            }
        }];
    }

    isAuthorization = !isAuthorization;

}

- (void)clickScanButtonAction
{
//    [self QRCodeScanVC:[[WCQRCodeScanningVC alloc] init]];

}

- (void)QRCodeScanVC:(UIViewController *)scanVC {
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (device) {
        AVAuthorizationStatus status = [AVCaptureDevice authorizationStatusForMediaType:AVMediaTypeVideo];
        switch (status) {
            case AVAuthorizationStatusNotDetermined: {
                [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
                    if (granted) {
                        dispatch_sync(dispatch_get_main_queue(), ^{
                            [self.navigationController pushViewController:scanVC animated:YES];
                        });
                        NSLog(@"用户第一次同意了访问相机权限 - - %@", [NSThread currentThread]);
                    } else {
                        NSLog(@"用户第一次拒绝了访问相机权限 - - %@", [NSThread currentThread]);
                    }
                }];
                break;
            }
            case AVAuthorizationStatusAuthorized: {
                [self.navigationController pushViewController:scanVC animated:YES];
                break;
            }
            case AVAuthorizationStatusDenied: {
                UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"请去-> [设置 - 隐私 - 相机 - SGQRCodeExample] 打开访问开关" preferredStyle:(UIAlertControllerStyleAlert)];
                UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
                    
                }];
                
                [alertC addAction:alertA];
                [self presentViewController:alertC animated:YES completion:nil];
                break;
            }
            case AVAuthorizationStatusRestricted: {
                NSLog(@"因为系统原因, 无法访问相册");
                break;
            }
                
            default:
                break;
        }
        return;
    }
    
    UIAlertController *alertC = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"未检测到您的摄像头" preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *alertA = [UIAlertAction actionWithTitle:@"确定" style:(UIAlertActionStyleDefault) handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    
    [alertC addAction:alertA];
    [self presentViewController:alertC animated:YES completion:nil];
}


- (void)checkUserIsRegistSuccssRequestWith:(NSDictionary *)dic
{
    NSDictionary *checkdic = @{
                               @"uid":dic[@"unionId"],
                               @"name":dic[@"name"],
                               @"gender":dic[@"gender"],
                               @"iconurl":dic[@"iconurl"],
                               @"type":@"weixin"
                               };
    [NetWorkingManager postWithUrl:kCheckWChatRegist params:checkdic success:^(id json) {
        NSLog(@"%@",json);
        if ([json[@"code"] integerValue]>0) {
            jxt_showAlertOneButton(@"提示", @"是否注册?", @"确定", ^(NSInteger buttonIndex) {
                [self registUserWithWChat:dic];
            });
        }else{
            jxt_showAlertTitle(@"已经注册,登录成功");
        }
        
    } failure:^(NSDictionary *error) {
        
    }];
}

- (void)registUserWithWChat:(NSDictionary *)dic
{
    RegistVC *vc = [[RegistVC alloc] init];
    vc.wchat_parameters = dic;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)clickEvokeWChatXiaoChengxu
{
//    WXLaunchMiniProgramReq *launchMiniProgramReq = [WXLaunchMiniProgramReq object];
//    launchMiniProgramReq.userName = @"gh_71f051cc1914";  //拉起的小程序的username
//    launchMiniProgramReq.path = nil;    //拉起小程序页面的可带参路径，不填默认拉起小程序首页
//    launchMiniProgramReq.miniProgramType = 1; //拉起小程序的类型
//    BOOL success = [WXApi sendReq:launchMiniProgramReq];
    
    
    //设置用户自定义的平台
    [UMSocialUIManager setPreDefinePlatforms:@[
                                               @(UMSocialPlatformType_WechatSession),
                                               
                                               ]];
    //设置分享面板的显示和隐藏的代理回调
    __weak typeof(self)weakSelf = self;

    [UMSocialUIManager setShareMenuViewDelegate:weakSelf];
    if ([WXApi isWXAppInstalled]) {
        NSLog(@"=====");
    }
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        
    }];
    
    
}
- (void)UMSocialShareMenuViewDidAppear
{
    NSLog(@"页面出现");
}

-(void)onResp:(BaseResp *)resp
{
    if ([resp isKindOfClass:[WXLaunchMiniProgramResp class]])
    {
        WXLaunchMiniProgramResp *back_resp = (WXLaunchMiniProgramResp *)resp;
        NSString *string = back_resp.extMsg;
        // 对应JsApi navigateBackApplication中的extraData字段数据
        NSLog(@"吊起小程序回调====%@",string);
    }
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark ------懒加载
- (UIButton *)wChatBtn{
    if(!_wChatBtn){
        _wChatBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"微信登录" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor blueColor];
            [object addTarget:self action:@selector(getAuthWithUserInfoFromUM:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 100;
            object;
        });
    }
    return _wChatBtn;
}

- (UIButton *)scanBtn{
    if(!_scanBtn){
        _scanBtn = ({
            UIButton * object = [[UIButton alloc]init];
            object.hidden = YES;
            [object setTitle:@"扫码登录" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor blueColor];
            [object addTarget:self action:@selector(clickScanButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _scanBtn;
}

- (UIButton *)QQBtn{
    if(!_QQBtn){
        _QQBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"QQ登录" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor blueColor];
            [object addTarget:self action:@selector(getAuthWithUserInfoFromUM:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 101;

            object;
       });
    }
    return _QQBtn;
}

- (UIButton *)sniaBtn{
    if(!_sniaBtn){
        _sniaBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"新浪登录" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor blueColor];
            [object addTarget:self action:@selector(getAuthWithUserInfoFromUM:) forControlEvents:UIControlEventTouchUpInside];
            object.tag = 102;

            object;
            
       });
    }
    return _sniaBtn;
}

- (UIButton *)evokeWchatBtn{
    if(!_evokeWchatBtn){
        _evokeWchatBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"唤起小程序" forState:UIControlStateNormal];
            object.backgroundColor = [UIColor blueColor];
            [object addTarget:self action:@selector(clickEvokeWChatXiaoChengxu) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _evokeWchatBtn;
}
@end
