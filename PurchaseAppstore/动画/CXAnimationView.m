//
//  CXAnimationView.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "CXAnimationView.h"
#define kCenter_img_size  CGSizeMake(21, 22)
#define kRight_img_size  CGSizeMake(25, 11)
#define kBottom_img_size CGSizeMake(65, 73)
@interface CXAnimationView ()
@property (nonatomic, strong) UIImageView *bottom_img;
@property (nonatomic, strong) UIImageView *center_img;
@property (nonatomic, strong) UIImageView *right_img;


@end
@implementation CXAnimationView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self addCXAnimationViewSubviews];
    }
    return self;
}
- (void)addCXAnimationViewSubviews
{
    [self addSubview:self.bottom_img];
    [self addSubview:self.right_img];
    [self addSubview:self.center_img];
}
- (void)handleCXAnimationViewWithBottomImage:(NSString *)bottom_img centerImage:(NSString *)center_img rightImage:(NSString *)right_img
{
    [self.bottom_img setImage:[UIImage imageNamed:bottom_img]];
    [self.center_img setImage:[UIImage imageNamed:center_img]];
    [self.right_img setImage:[UIImage imageNamed:right_img]];
}
- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    NSLog(@"%@",NSStringFromCGPoint(point));
    if (!self.isUserInteractionEnabled || self.isHidden || self.alpha <= 0.01) {
            return nil;
        }

    for (UIView *subview in [self.subviews reverseObjectEnumerator]) {
        CGPoint convertedPoint = [subview convertPoint:point fromView:self];
        UIView *hitTestView = [subview hitTest:convertedPoint withEvent:event];
        if (hitTestView) {
            return hitTestView;
        }
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (UIImageView *)bottom_img{
    if(!_bottom_img){
        _bottom_img = ({
            UIImageView * object = [[UIImageView alloc]initWithFrame:self.bounds];
            object;
       });
    }
    return _bottom_img;
}

- (UIImageView *)center_img{
    if(!_center_img){
        _center_img = ({
            UIImageView * object = [[UIImageView alloc]initWithFrame:CGRectMake(0, 30, kCenter_img_size.width, kCenter_img_size.height)];
            object.center = CGPointMake(self.bottom_img.center.x, self.bottom_img.center.y-14);
            CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            anim.fromValue = [NSNumber numberWithFloat:-M_PI_4];
            anim.toValue = [NSNumber numberWithFloat:M_PI_4];
            anim.autoreverses = YES;
            anim.duration = 0.5;
            anim.repeatCount = HUGE_VALF;
            [object.layer addAnimation:anim forKey:@"rotationAnimation"];
            object;
       });
    }
    return _center_img;
}

- (UIImageView *)right_img{
    if(!_right_img){
        _right_img = ({
            UIImageView * object = [[UIImageView alloc]initWithFrame:CGRectMake(kBottom_img_size.width-kRight_img_size.width, 0, kRight_img_size.width, kRight_img_size.height)];
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
            // 起始帧和终了帧的设定
            animation.fromValue = [NSValue valueWithCGPoint:object.layer.position]; // 起始帧
            animation.toValue = [NSValue valueWithCGPoint:self.center_img.center];
            
            // 设定为缩放
            CABasicAnimation *scale_animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
             
            // 缩放倍数
            scale_animation.fromValue = [NSNumber numberWithFloat:1]; // 开始时的倍率
            scale_animation.toValue = [NSNumber numberWithFloat:0.5]; // 结束时的倍率
             
            //动画组
            CAAnimationGroup *animGroup = [CAAnimationGroup animation];
            animGroup.animations = [NSArray arrayWithObjects:animation, scale_animation, nil];
            animGroup.duration = 1;
            animGroup.repeatCount = HUGE_VALF;
            animGroup.autoreverses = YES;
            // 添加动画
            [object.layer addAnimation:animGroup forKey:nil];            object;
       });
    }
    return _right_img;
}
@end
