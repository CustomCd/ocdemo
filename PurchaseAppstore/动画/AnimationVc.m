//
//  AnimationVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import "AnimationVc.h"
#import "CXAnimationView.h"
#define Scale_num 1
@interface AnimationVc ()
@property (nonatomic, strong) UIImageView *animation_bottom;
@property (nonatomic, strong) UIImageView *middle_img;
@property (nonatomic, strong) UIImageView *right_img;
@property (nonatomic, strong) CXAnimationView *animationView;
@property (nonatomic, strong) UIView *shadow_view;



@end

@implementation AnimationVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.animationView];
    [self.view addSubview:self.shadow_view];
}
- (void)clickOutSideViewAction
{
    NSLog(@"点击父类视图外");
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIImageView *)animation_bottom{
    if(!_animation_bottom){
        _animation_bottom = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.image = [UIImage imageNamed:@"红包"];
            [object addSubview:self.right_img];
            [object addSubview:self.middle_img];
            [self.middle_img mas_makeConstraints:^(MASConstraintMaker *make) {
                make.centerX.equalTo(object.mas_centerX);
                make.top.equalTo(object).offset(15);
                make.size.mas_equalTo(CGSizeMake(21*Scale_num, 22*Scale_num));
            }];
            object;
       });
    }
    return _animation_bottom;
}

- (UIImageView *)middle_img{
    if(!_middle_img){
        _middle_img = ({
            UIImageView * object = [[UIImageView alloc] init];
            object.image = [UIImage imageNamed:@"领"];
            CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
            anim.fromValue = [NSNumber numberWithFloat:-M_PI_4];
            anim.toValue = [NSNumber numberWithFloat:M_PI_4];
            anim.autoreverses = YES;
            anim.duration = 0.5;
            anim.repeatCount = HUGE_VALF;
            [object.layer addAnimation:anim forKey:@"rotationAnimation"];
            object;
       });
    }
    return _middle_img;
}

- (UIImageView *)right_img{
    if(!_right_img){
        _right_img = ({
            UIImageView * object = [[UIImageView alloc]initWithFrame:CGRectMake((65-25)*Scale_num, 0, 25*Scale_num, 11*Scale_num)];
            object.image = [UIImage imageNamed:@"现金"];
            CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];

            // 起始帧和终了帧的设定
            animation.fromValue = [NSValue valueWithCGPoint:object.layer.position]; // 起始帧
            animation.toValue = [NSValue valueWithCGPoint:CGPointMake(65/2*Scale_num, 20*Scale_num)]; // 终了帧
            

            // 设定为缩放
            CABasicAnimation *scale_animation = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
             
            // 缩放倍数
            scale_animation.fromValue = [NSNumber numberWithFloat:1]; // 开始时的倍率
            scale_animation.toValue = [NSNumber numberWithFloat:0.5]; // 结束时的倍率
             
            //动画组
            CAAnimationGroup *animGroup = [CAAnimationGroup animation];
            animGroup.animations = [NSArray arrayWithObjects:animation, scale_animation, nil];
            animGroup.duration = 1;
            animGroup.repeatCount = HUGE_VALF;
            animGroup.autoreverses = YES;
            // 添加动画
            [object.layer addAnimation:animGroup forKey:nil];
            object;
       });
    }
    return _right_img;
}


- (CXAnimationView *)animationView{
    if(!_animationView){
        _animationView = ({
            CXAnimationView * object = [[CXAnimationView alloc]initWithFrame:CGRectMake(0, 0, 65*Scale_num, 73*Scale_num)];
            object.center = self.view.center;
            [object handleCXAnimationViewWithBottomImage:@"红包" centerImage:@"领" rightImage:@"现金"];
            object;
       });
    }
    return _animationView;
}

- (UIView *)shadow_view{
    if(!_shadow_view){
        _shadow_view = ({
            UIView * object = [[UIView alloc]init];
            object.backgroundColor = [UIColor whiteColor];
            CGRect rect = CGRectMake(30, 150, 100, 100);
            object.frame = rect;
            // spread 对应 Sketch 里阴影的 “扩展”，值是 10
            rect = CGRectInset(CGRectMake(0, 0, 100, 100), -10, -10);
            object.layer.shadowPath = [UIBezierPath bezierPathWithRect:rect].CGPath;
            // 颜色是黑色（ #000000 ）
            object.layer.shadowColor = [UIColor blackColor].CGColor;
            // alpha 50
            object.layer.shadowOpacity = 0.5;
            // X: 0  Y: 10
            object.layer.shadowOffset = CGSizeMake(0, 10);
            // 对应 Sketch 里阴影的 “模糊” 的设置，值是 20 / 2 = 10
            object.layer.shadowRadius = 10;
             object;
       });
    }
    return _shadow_view;
}
@end
