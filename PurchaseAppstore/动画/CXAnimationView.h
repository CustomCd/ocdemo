//
//  CXAnimationView.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CXAnimationView : UIView
- (void)handleCXAnimationViewWithBottomImage:(NSString *)bottom_img
                                 centerImage:(NSString *)center_img
                                  rightImage:(NSString *)right_img;
@end

NS_ASSUME_NONNULL_END
