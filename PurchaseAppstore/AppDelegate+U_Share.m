//
//  AppDelegate+U_Share.m
//  DBClient
//
//  Created by chen xin on 2017/12/22.
//  Copyright © 2017年 daban. All rights reserved.
//

#import "AppDelegate+U_Share.h"
#import <UMCShare/UMShare/UMShare.h>
#import <UMCommon/UMCommon.h>
#define USHARE_DEMO_APPKEY @"5b9b86cef43e48479c000067"
//#define USHARE_DEMO_APPKEY @"58ca4d2b2ae85b2b080005de"



#define K_WeixinAppID @"wx5270d2d7ffe1bc91"
#define K_WeixinAppSecret @"b1308f92124166f63aebe4d9ee7e9200"

//#define K_QQKey @"1105579589"
//#define K_QQAppSecret @"6rjh7D4gc5J36A4e"
#define K_QQKey @"1105974895"
#define K_QQAppSecret @"658zF2npb35bkHZn"

#define K_SinaKey @"199335694"
#define K_SinaAppSecret @"97c9a68faa6e0035d25d819cb943ddae"


@implementation AppDelegate (U_Share)

- (void)star_Umen_application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions{
    /* 打开日志 */
    [[UMSocialManager defaultManager] openLog:YES];
    [UMSocialGlobal shareInstance].isClearCacheWhenGetUserInfo = YES;
    /* 设置友盟appkey */
//    [[UMSocialManager defaultManager] setUmSocialAppkey:USHARE_DEMO_APPKEY];
//    [UMSocialManager]
    [UMConfigure initWithAppkey:USHARE_DEMO_APPKEY channel:@"App Store"];
    [UMConfigure setLogEnabled:YES];
    [self configUSharePlatforms];
    
    
    
//    UMConfigInstance.channelId = @"App Store";
//    [MobClick startWithConfigure:UMConfigInstance];//配置以上参数后调用此方法初始化SDK！
//    [MobClick setLogEnabled:YES];
    
}
- (void)configUSharePlatforms
{
    /* 设置微信的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:K_WeixinAppID appSecret:K_WeixinAppSecret redirectURL:@"http://mobile.umeng.com/social"];
    
 
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_QQ appKey:K_QQKey/*设置QQ平台的appID*/  appSecret:nil redirectURL:@"http://mobile.umeng.com/social"];
    
    /* 设置新浪的appKey和appSecret */
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_Sina appKey:K_SinaKey  appSecret:K_SinaAppSecret redirectURL:@"https://sns.whalecloud.com/sina2/callback"];

    

}
// 支持所有iOS系统
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
        //return [[PayApi sharedApi] handleOpenURL:url];
    }
    return result;
    
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
//        [[PayApi sharedApi] handleOpenURL:url];
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
//        [[PayApi sharedApi] handleOpenURL:url];
    }
    return result;
}
@end
