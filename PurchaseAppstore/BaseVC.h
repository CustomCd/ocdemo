//
//  BaseVC.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Masonry/Masonry.h>
#import "JXTAlertManagerHeader.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import <MJRefresh/MJRefresh.h>
#import "MBProgressHUD.h"
#import "NetWorkingManager.h"

//安全解析字符串 数组 字典
#define SAFESTRING(str)  ( ( ((str)!=nil)&&![(str) isKindOfClass:[NSNull class]])?[NSString stringWithFormat:@"%@",(str)]:@"" )
#define SAFENSARRY(arr) [arr isKindOfClass:[NSArray class]] ?arr:@[]
#define SAFENSDICTIONARY(dic) [dic isKindOfClass:[NSDictionary class]]?dic:@{}
#define kSCREEN_WIDTH ([UIScreen mainScreen].bounds.size.width)
#define kSCREEN_HEIGHT ([UIScreen mainScreen].bounds.size.height)
#define KIsIPhoneX ((kSCREEN_HEIGHT == 812.f || kSCREEN_HEIGHT == 896.f) ? YES : NO)

@interface BaseVC : UIViewController
@property (nonatomic, assign) NSInteger current_page;
- (void)handleTotalPageOfListDataWithTableview:(UITableView *)tableview list_data:(id)list_data;
- (void)setTableviewConfigWith:(UITableView *)tableview;
- (void)addKeyboardDismissGesture;
@end
