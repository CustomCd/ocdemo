//
//  HideNavigationbarLineVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/9/23.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "HideNavigationbarLineVc.h"

//颜色
#define kUIColorFromRGB(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]
@interface HideNavigationbarLineVc ()
@property (nonatomic, strong) UIImageView *redBackgroud;

@end

@implementation HideNavigationbarLineVc

- (void)viewWillAppear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:kUIColorFromRGB(0xE84548)];
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}
- (void)viewWillDisappear:(BOOL)animated
{
    [self.navigationController.navigationBar setBarTintColor:[UIColor whiteColor]];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    

    [self.view addSubview:self.redBackgroud];
    [self.redBackgroud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.mas_equalTo(84);
    }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/



- (UIImageView *)redBackgroud{
    if(!_redBackgroud){
        _redBackgroud = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.image = [UIImage imageNamed:@"navigationabr"];
            object;
       });
    }
    return _redBackgroud;
}
@end
