//
//  GuidanceView.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/22.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GuidanceManager : UIView
+ (void)guidanceManagerConfiguration;
@end

NS_ASSUME_NONNULL_END
