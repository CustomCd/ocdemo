//
//  GuidanceView.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/22.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "GuidanceManager.h"

@implementation GuidanceManager

-(void)dealloc
{
    NSLog(@"GuidanceManager释放了");
}
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        [self addGuidanceViewSubviews];
    }
    return self;
}
- (void)addGuidanceViewSubviews
{
    CGFloat scale = self.bounds.size.width/375;
    CGFloat width = 375*scale;
    CGFloat height = 667*scale-5;
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    [self addSubview:scroll];
    scroll.center = CGPointMake(scroll.center.x, self.center.y);

    
    for (int i = 0; i<3; i++) {
        CGFloat x = i*width;
        UIImageView *subview = [[UIImageView alloc] initWithFrame:CGRectMake(x, 0, width, height)];
        subview.image = [UIImage imageNamed:[NSString stringWithFormat:@"引导页%d",i+1]];
        [scroll addSubview:subview];
        if (i == 2) {
            UIButton *tiyanButton = [UIButton buttonWithType:UIButtonTypeCustom];
            tiyanButton.frame = CGRectMake((width-104)/2, height-80-36, 104, 36);
            [subview addSubview:tiyanButton];
            [tiyanButton setBackgroundImage:[UIImage imageNamed:@"lijitiyan"] forState:UIControlStateNormal];
            [tiyanButton setBackgroundImage:[UIImage imageNamed:@"lijitiyan"] forState:UIControlStateHighlighted];
            [tiyanButton addTarget:self action:@selector(removeSelfEnterMainView) forControlEvents:UIControlEventTouchUpInside];
            subview.userInteractionEnabled = YES;
        }
        
    }
    scroll.contentSize = CGSizeMake(width*3, height);
    scroll.pagingEnabled = YES;
    scroll.bounces = NO;
    scroll.showsHorizontalScrollIndicator = NO;
}
- (void)removeSelfEnterMainView
{
    [self removeFromSuperview];
}


+ (void)saveCurrentLocalVersion:(NSString *)version
{
    [[NSUserDefaults standardUserDefaults] setObject:version forKey:@"version"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
+ (NSString *)getCurrentLocalVersion
{
    NSString *version = [[NSUserDefaults standardUserDefaults] objectForKey:@"version"];
    return version;
}


#pragma --------------public
+ (void)guidanceManagerConfiguration
{
    NSString *saveVersion = [GuidanceManager getCurrentLocalVersion];
    NSString *currentVersion = [[[NSBundle mainBundle] infoDictionary]objectForKey:@"CFBundleShortVersionString"];
    if ([saveVersion isEqualToString:currentVersion]) {
        return;
    }
    UIWindow *currentWindow = [[[UIApplication sharedApplication] delegate]window];
    GuidanceManager *guide = [[GuidanceManager alloc] initWithFrame:currentWindow.bounds];
    [currentWindow addSubview:guide];
    [GuidanceManager saveCurrentLocalVersion:currentVersion];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
