//
//  ShareDocumentVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/11/5.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "ShareDocumentVC.h"
#import <UMSocialWechatHandler.h>
#import <UMCShare/WXApi.h>
#import <Masonry/Masonry.h>

@interface ShareDocumentVC ()
@property (nonatomic, strong) UIButton *documentShareBtn;

@end

@implementation ShareDocumentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"分享文件";
    
    [self.view addSubview:self.documentShareBtn];
    [self.documentShareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200, 40));
    }];


    
}


- (void)clickDocumentShareBtnAction
{
    BOOL success_  = [WXApi openWXApp];
    NSLog(@"%d",success_);
    return;
    //文件数据
    WXFileObject *fileObj = [WXFileObject object];
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"interface" ofType:@"doc"];
    fileObj.fileData = [NSData dataWithContentsOfFile:filePath];
    NSString *fileTpe = @"doc";
    //多媒体消息
    fileObj.fileExtension = fileTpe;
    WXMediaMessage *wxMediaMessage = [WXMediaMessage message];
    wxMediaMessage.title = [NSString stringWithFormat:@"文件.%@",fileTpe];
    wxMediaMessage.description = @"描述";
    wxMediaMessage.messageExt = fileTpe;
    wxMediaMessage.mediaObject = fileObj;
    //发送消息
    SendMessageToWXReq *req = [[SendMessageToWXReq alloc] init];
    req.message = wxMediaMessage;
    req.bText = NO;
    req.scene = WXSceneSession;
    [WXApi sendReq:req];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIButton *)documentShareBtn{
    if(!_documentShareBtn){
        _documentShareBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"分享文件到微信" forState:UIControlStateNormal];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            [object addTarget:self action:@selector(clickDocumentShareBtnAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _documentShareBtn;
}
@end
