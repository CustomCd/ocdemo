//
//  AppDelegate.m
//  DBClient
//
//  Created by chen xin on 2017/11/17.
//  Copyright © 2017年 daban. All rights reserved.
//


#import "NetWorkingManager.h"
#import <AFNetworking/AFNetworking.h>
#import "AppDelegate.h"
#import <SVProgressHUD/SVProgressHUD.h>
#define iOS_Version [[[UIDevice currentDevice] systemVersion] floatValue]
#define HOST @""
#define PORT @""
@implementation NetWorkingManager

+ (void)postWithUrl:(NSString *)url
             params:(NSDictionary *)params
            success:(void (^)(id))success
            failure:(void (^)(NSDictionary *))failure
{
    NSString *interfaceUrl = [NSString stringWithFormat:@"%@%@%@",HOST,PORT,url];
    
    AFHTTPSessionManager *manager = [NetWorkingManager AFNetworkingManagerCofig];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 15;
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    NSMutableDictionary *handlParameters = [NSMutableDictionary dictionaryWithDictionary:params];;
    
    /**
     *  加载动画添加
     */
	
	UIWindow *window = [[[UIApplication sharedApplication] delegate] window];

    [manager POST:interfaceUrl parameters:[handlParameters copy] progress:^(NSProgress * _Nonnull downloadProgress) {
        
    }success:^(NSURLSessionDataTask *operation, id responseObject) {
//        NSLog(@"post 请求%@==",responseObject);
        
        NSString *successCode = [NSString stringWithFormat:@"%@", responseObject[@"error_code"]];

        if ([successCode integerValue] > 0 &&
            successCode.length != 0 ) {
            
            NSLog(@"%@=错误描述===%@",interfaceUrl,responseObject);

        }


        
        if (success) {
            success(responseObject);
        }

        
    } failure:^(NSURLSessionDataTask *operation, NSError *error) {
        
        NSLog(@"%@= 请求失败%@",interfaceUrl,error);
        NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSDictionary *failureDic;
        if (data ==nil) {
            failureDic = nil;
        }else{
            failureDic = (NSDictionary *)[NetWorkingManager dataToArrayOrDictionary:data];
            
        }
        if (failure) {
            failure(failureDic);
        }
//        [[CustomAlert sharedInstance] alertWith:@"网络超时"];
//        if (showHud) {
//            [MBProgressHUD hideHUDForView:window animated:YES];
//        }else{
//            [MBProgressHUD hideHUDForView:window animated:NO];
//        }
		
		
    }];
}

+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params formData:(NSArray *)formDataArray success:(void (^)(id))success failure:(void (^)(NSDictionary *))failure
{
    NSString *interfaceUrl = [NSString stringWithFormat:@"%@%@%@",HOST,PORT,url];
    
    AFHTTPSessionManager *manager = [NetWorkingManager AFNetworkingManagerCofig];
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    manager.requestSerializer.timeoutInterval = 60;
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    /**
     *  加载动画添加
     */
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    [manager POST:interfaceUrl parameters:params constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        for (NSDictionary *dic in formDataArray) {
            [formData appendPartWithFileData:dic[@"data"] name:@"attachment" fileName:[@"yimiaotong." stringByAppendingString:dic[@"type"]]  mimeType:dic[@"type"]];
        }
//        for (NSData * myFormData in formDataArray) {
//            [formData appendPartWithFileData:myFormData name:@"attachment" fileName:@"yimiaotong.mp3" mimeType:@"mp3"];
//        }
        
    }progress:^(NSProgress * _Nonnull downloadProgress) {
        
    }success:^(NSURLSessionDataTask * operation, id responseObject) {

        NSLog(@"请求成功===%@",responseObject);

        if (success) {
            success(responseObject);
        }
        
        
    } failure:^(NSURLSessionDataTask * operation, NSError *error) {
        NSLog(@"请求失败===%@",error);

        NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"];
        NSDictionary *failureDic;
        if (data ==nil) {
            failureDic = nil;
        }else{
            failureDic = (NSDictionary *)[NetWorkingManager dataToArrayOrDictionary:data];
            
        }
        if (failure) {
            failure(failureDic);
        }
        
    }];
}

#pragma mark ---------私有方法--------------

#pragma mark ---------- 是否有alert显示
+ (BOOL)isAlertShowNow
{
    if (iOS_Version >= 7.0) {
        if ([[UIApplication sharedApplication].keyWindow isMemberOfClass:[UIWindow class]])
        {
            return NO;
        }
        return YES;
    }
    else {
        for (UIWindow* window in [UIApplication sharedApplication].windows) {
            NSArray* subviews = window.subviews;
            if ([subviews count] > 0)
                if ([[subviews objectAtIndex:0] isKindOfClass:[UIAlertView class]] || [[subviews objectAtIndex:0] isKindOfClass:[UIActionSheet class]])
                    return YES;
        }
        return NO;
    }
}

//jsonData转成字典或数组
+ (instancetype )dataToArrayOrDictionary:(NSData *)jsonData
{
    
    NSError *error = nil;
    id jsonObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                    options:NSJSONReadingAllowFragments
                                                      error:&error];
    if (jsonObject != nil && error == nil){
        return jsonObject;
    }else{
        // 解析错误
        return nil;
    }
    
}

+ (void)checkNetworkReachabilityBlock:(void (^)(BOOL networkstatus))networkstatus
{
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    [manager startMonitoring];
    
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        switch (status) {
            case AFNetworkReachabilityStatusUnknown:
            {
                //未知网络
                NSLog(@"未知网络");
                networkstatus(NO);
            }
                break;
            case AFNetworkReachabilityStatusNotReachable:
            {
                //无法联网
                NSLog(@"无法联网");
                networkstatus(NO);
                
            }
                break;
                
            case AFNetworkReachabilityStatusReachableViaWWAN:
            {
                //手机自带网络
                NSLog(@"当前使用的是2g/3g/4g网络");
                networkstatus(YES);
                
            }
                break;
            case AFNetworkReachabilityStatusReachableViaWiFi:
            {
                //WIFI
                NSLog(@"当前在WIFI网络下");
                networkstatus(YES);
            }
                break;
                
                
        }
    }];
}

+ (AFHTTPSessionManager *)AFNetworkingManagerCofig
{
    
    AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    AFHTTPSessionManager *manager = [app sharedHTTPSession];
    

    AFJSONResponseSerializer *serializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer = serializer;
    [serializer setRemovesKeysWithNullValues:YES];
    [manager setResponseSerializer:serializer];
	[manager setRequestSerializer:[AFJSONRequestSerializer serializer]];
    manager.responseSerializer.acceptableContentTypes= [NSSet setWithObjects:@"application/json",@"text/json",@"text/javascript",@"text/html",@"multipart/form-data",nil];
    
    
    return manager;
}







@end
