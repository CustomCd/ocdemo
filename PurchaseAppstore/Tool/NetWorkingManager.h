//
//  AppDelegate.m
//  DBClient
//
//  Created by chen xin on 2017/11/17.
//  Copyright © 2017年 daban. All rights reserved.
//


#import <Foundation/Foundation.h>

#define PPTHOST @"http://47.93.240.170"
@interface NetWorkingManager : NSObject

//post 请求
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params success:(void (^)(id json))success failure:(void (^)(NSDictionary *error))failure;
//上传
+ (void)postWithUrl:(NSString *)url params:(NSDictionary *)params formData:(NSArray *)formDataArray success:(void (^)(id json))success failure:(void (^)(NSDictionary *error))failure;

@end
