//
//  SelectMoreVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/11/8.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "SelectMoreVc.h"
#import <Masonry/Masonry.h>
@interface SelectMoreVc ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *selectMore_tableview;
@property (nonatomic, strong) NSMutableArray *data_arr;

@end

@implementation SelectMoreVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.selectMore_tableview];
    [self.selectMore_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.selectMore_tableview reloadData];
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithTitle:@"编辑" style:UIBarButtonItemStyleDone target:self action:@selector(rightBarItemClick:)];
    self.navigationItem.rightBarButtonItem = item;
}


 #pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data_arr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
    }
    cell.textLabel.text = self.data_arr[indexPath.row];
    return cell;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleDelete | UITableViewCellEditingStyleInsert;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView.isEditing) {
        return;
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    return YES;
    
}


- (void)rightBarItemClick:(UIBarButtonItem *)item{
    if ([item.title isEqualToString:@"编辑"]) {
        if (self.data_arr.count == 0) {
            return;
        }
        item.title = @"取消";
        [self.selectMore_tableview setEditing:YES animated:YES];
    }else{
        item.title = @"编辑";
        [self.selectMore_tableview setEditing:NO animated:YES];
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)selectMore_tableview{
    if(!_selectMore_tableview){
        _selectMore_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource= self;
            object.rowHeight = 44;
            object.allowsSelectionDuringEditing = YES;
            object.allowsMultipleSelectionDuringEditing = YES;
            object;
       });
    }
    return _selectMore_tableview;
}

- (NSMutableArray *)data_arr{
    if(!_data_arr){
        _data_arr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            for (int i = 0 ; i<9; i++) {
                [object addObject:[NSString stringWithFormat:@"测试+%d",i]];
            }
            object;
       });
    }
    return _data_arr;
}
@end
