//
//  AppDelegate.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/14.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AFNetworking/AFNetworking.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, assign) BOOL isSupportHori;

- (AFHTTPSessionManager *)sharedHTTPSession;


@end

