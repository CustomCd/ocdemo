//
//  EmojiVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/6/20.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "EmojiVc.h"
#import "NetWorkingManager.h"
#import "YMTInputToolView.h"
#define EMOJI_CODE_TO_SYMBOL(x) ((((0x808080F0 | (x & 0x3F000) >> 4) | (x & 0xFC0) << 10) | (x & 0x1C0000) << 18) | (x & 0x3F) << 24);
@interface EmojiVc ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate>
@property (nonatomic, strong) UITableView *emojiTabelview;
@property (nonatomic, strong) NSMutableArray *emojiArr;
@property (nonatomic, strong) UITextField *input;
@property (nonatomic, strong) UITextView *textview;
@property (nonatomic, strong) YMTInputToolView *input_tool;
@property (nonatomic, strong) UIButton *testBtn;

@end

@implementation EmojiVc

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboradFrameDidShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboradFrameDidHidden:) name:UIKeyboardWillHideNotification object:nil];
    
}
- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];

}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    NSLog(@"%@",[self defaultEmoticons]);
//    [self.view addSubview:self.emojiTabelview];
//    [self.emojiTabelview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.view);
//    }];
    [self.view addSubview:self.testBtn];
    [self.testBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.view).offset(100);
        make.top.equalTo(self.view).offset(100);
        make.size.mas_equalTo(CGSizeMake(100, 40));
    }];
    [self.view addSubview:self.textview];
    [self.textview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    [self.view addSubview:self.input_tool];

    return;
    _input  = [[UITextField alloc] init];
    _input.layer.borderWidth = 1;

    [self.view addSubview: _input];
    [_input mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.view).offset(100);
        make.height.mas_equalTo(0);
    }];
    [_input addTarget:self action:@selector(textfieldChange) forControlEvents:UIControlEventEditingChanged];
    [self getEmojText];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(_input.mas_bottom).offset(20);
        make.size.mas_equalTo(CGSizeMake(80, 40));
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    [button addTarget:self action:@selector(sendEmoj) forControlEvents:UIControlEventTouchUpInside];
}





- (void)clickTestBtn
{
    [self.input_tool showInputToolWithPlaceHolder:@"请输入回复"];

}

#pragma mark ----------监听键盘

- (void)keyboradFrameDidShow:(NSNotification *)notification
{
    NSDictionary *info = notification.userInfo;
    CGFloat keyboard_y = [info[UIKeyboardFrameEndUserInfoKey] CGRectValue].origin.y;
    NSTimeInterval time =  [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    // Adjust the content inset of suggestion view
    [UIView animateWithDuration:time animations:^{
        CGFloat y = keyboard_y-60- 69;
        self.input_tool.frame = CGRectMake(0, y, [UIScreen mainScreen].bounds.size.width, 69);
    }completion:^(BOOL finished) {
        
        
    }];
}

- (void)keyboradFrameDidHidden:(NSNotification *)notification
{
    NSDictionary *info = notification.userInfo;
    
    NSTimeInterval time =  [info[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:time animations:^{
        self.input_tool.frame = CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, [UIScreen mainScreen].bounds.size.width, 69);

    }];
}










- (void)sendEmoj
{
    [NetWorkingManager postWithUrl:@"" params:@{@"content":_input.text} formData:nil success:^(id json) {
        
    } failure:^(NSDictionary *error) {
        
    }];
//    [NetWorkingManager postWithUrl: params:@{@"content":_input.text} success:^(id json) {
//        NSLog(@"上传=====%@",json);
//    } failure:^(NSDictionary *error) {
//
//    }];
}
- (void)getEmojText
{
    [NetWorkingManager postWithUrl:@"" params:nil success:^(id json) {
        NSLog(@"获取 emoji==== %@",json);
        NSDictionary *dic = (NSDictionary *)json;
        _input.text = json[@"content"];
    } failure:^(NSDictionary *error) {
        
    }];
}

- (void)textfieldChange
{
    
    NSString *uniStr = [NSString stringWithUTF8String:[_input.text UTF8String]];
    NSData *uniData = [uniStr dataUsingEncoding:NSNonLossyASCIIStringEncoding];
    NSString *goodStr = [[NSString alloc] initWithData:uniData encoding:NSUTF8StringEncoding] ;
    
   
    NSLog(@"%@",goodStr);
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.emojiArr.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.emojiArr[indexPath.row];
    return cell;
}

- (NSArray *)defaultEmoticons {
    
    NSMutableArray *array = [NSMutableArray new];
    
    for (int i=0x1F600; i<=0x1F64F; i++) {
        
        if (i < 0x1F641 || i > 0x1F644) {
            
            int sym = EMOJI_CODE_TO_SYMBOL(i);
            
            NSString *emoT = [[NSString alloc] initWithBytes:&sym length:sizeof(sym) encoding:NSUTF8StringEncoding];
            
            [array addObject:emoT];
            
        }
        
    }
    
    return array;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%@",self.emojiArr[indexPath.row]);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/




- (UITableView *)emojiTabelview{
    if(!_emojiTabelview){
        _emojiTabelview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object;
       });
    }
    return _emojiTabelview;
}

- (NSMutableArray *)emojiArr{
    if(!_emojiArr){
        _emojiArr = ({
            NSMutableArray * object = [[NSMutableArray alloc]init];
            [object addObjectsFromArray:[ self defaultEmoticons]];
            object;
       });
    }
    return _emojiArr;
}

- (UITextView *)textview{
    if(!_textview){
        _textview = ({
            UITextView * object = [[UITextView alloc]init];
            object.returnKeyType = UIReturnKeyDone;
            object.layer.borderWidth = 1;
            object;
       });
    }
    return _textview;
}
- (YMTInputToolView *)input_tool{
    if(!_input_tool){
        _input_tool = ({
            YMTInputToolView * object = [[YMTInputToolView alloc]init];
            object.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 69);
            object.getInputConentBlock = ^(NSString *input) {
            };
            object;
        });
    }
    return _input_tool;
}

- (UIButton *)testBtn{
    if(!_testBtn){
        _testBtn = ({
            UIButton * object = [[UIButton alloc]init];
            object.layer.borderWidth = 1;
            [object addTarget:self action:@selector(clickTestBtn) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _testBtn;
}
@end
