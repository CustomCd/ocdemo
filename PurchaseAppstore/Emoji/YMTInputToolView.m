//
//  YMTInputToolView.m
//  vaccineinfo
//
//  Created by 亿苗通 on 2018/11/29.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "YMTInputToolView.h"
#import <Masonry/Masonry.h>
@interface YMTInputToolView()<UITextViewDelegate>
{
    NSString *_textviewPlaceholder;

}
@property (nonatomic, strong) UITextView *input_textview;
@property (nonatomic, strong) UIButton *publishBtn;
@property (nonatomic, strong) UILabel *placeholder;

@end
@implementation YMTInputToolView
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor lightGrayColor];
        [self addYMTInputToolViewSubviews];
    }
    return self;
}
- (void)addYMTInputToolViewSubviews
{
    [self addSubview:self.input_textview];
    [self addSubview:self.publishBtn];
    [self.publishBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.bottom.top.equalTo(self);
        make.width.mas_equalTo(65);
    }];
    [self.input_textview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.publishBtn.mas_left);
        make.top.equalTo(self).offset(7);
        make.bottom.equalTo(self).offset(-7);
        make.left.equalTo(self).offset(16);
    }];
}


- (void)clickPublishButtonAction
{
    [self.input_textview resignFirstResponder];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.getInputConentBlock) {
            self.getInputConentBlock(self.input_textview.text);
        }
        self.input_textview.text = @"";
        self.placeholder.hidden = NO;
    });

    
}
#pragma mark ------textviewdelegate
- (void)textViewDidChange:(UITextView *)textView
{
    self.publishBtn.enabled = textView.text.length >0 ?YES:NO;
    self.placeholder.hidden = textView.text.length >0 ?YES:NO;
}


#pragma mark ----------public
- (void)showInputToolWithPlaceHolder:(NSString *)placeholder
{

    self.placeholder.text = placeholder;

    [self.input_textview becomeFirstResponder];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self.superview endEditing:YES];
        return NO;//这里返回NO，就代表return键值失效，即页面上按下return，不会出现换行，如果为yes，则输入页面会换行
    }
    return YES;
}


- (UITextView *)input_textview{
    if(!_input_textview){
        _input_textview = ({
            UITextView * object = [[UITextView alloc]init];
            object.backgroundColor = [UIColor whiteColor];
            object.layer.cornerRadius = 4;
            object.layer.borderWidth = 1;
            object.delegate = self;
            object.returnKeyType = UIReturnKeyDone;
            [object addSubview:self.placeholder];
            [self.placeholder mas_makeConstraints:^(MASConstraintMaker *make) {
                make.top.equalTo(object).offset(8);
                make.left.equalTo(object).offset(5);
                make.right.equalTo(object);
                make.height.mas_equalTo(17);
            }];
//            self.placeholder.layer.borderWidth = 1;
            object;
       });
    }
    return _input_textview;
}

- (UIButton *)publishBtn{
    if(!_publishBtn){
        _publishBtn = ({
            UIButton * object = [[UIButton alloc]init];
            [object setTitle:@"发布" forState:UIControlStateNormal];
            [object setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
            [object setTitleColor:[UIColor lightGrayColor] forState:UIControlStateDisabled];
            [object addTarget:self action:@selector(clickPublishButtonAction) forControlEvents:UIControlEventTouchUpInside];
            object.enabled = NO;
            object;
       });
    }
    return _publishBtn;
}

- (UILabel *)placeholder{
    if(!_placeholder){
        _placeholder = ({
            UILabel * object = [[UILabel alloc] init];
            
            object;
       });
    }
    return _placeholder;
}
@end
