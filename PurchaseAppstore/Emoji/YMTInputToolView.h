//
//  YMTInputToolView.h
//  vaccineinfo
//
//  Created by 亿苗通 on 2018/11/29.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^GetInputConentBlock)(NSString *input);
@interface YMTInputToolView : UIView
@property (nonatomic, copy) GetInputConentBlock getInputConentBlock;

- (void)showInputToolWithPlaceHolder:(NSString *)placeHolder;
@end
