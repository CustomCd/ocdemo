//
//  BaseDataModel.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/11/13.
//  Copyright © 2019 亿苗通. All rights reserved.
//

#import <Foundation/Foundation.h>

//安全解析字符串 数组 字典
#define SAFESTRING(str)  ( ( ((str)!=nil)&&![(str) isKindOfClass:[NSNull class]])?[NSString stringWithFormat:@"%@",(str)]:@"" )
#define SAFENSARRY(arr) [arr isKindOfClass:[NSArray class]] ?arr:@[]
#define SAFENSDICTIONARY(dic) [dic isKindOfClass:[NSDictionary class]]?dic:@{}
NS_ASSUME_NONNULL_BEGIN

@interface BaseDataModel : NSObject
- (void)parseJsonData:(NSDictionary *)dictionary;

@end

NS_ASSUME_NONNULL_END
