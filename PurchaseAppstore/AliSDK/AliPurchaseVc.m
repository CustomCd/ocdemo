//
//  AliPurchaseVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/11/29.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "AliPurchaseVc.h"
#import <AlipaySDK/AlipaySDK.h>
#import "NetWorkingManager.h"
@interface AliPurchaseVc ()

@end

@implementation AliPurchaseVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"支付";
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:button];
    [button mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
    }];
    [button addTarget:self action:@selector(clickButtonAction) forControlEvents:UIControlEventTouchUpInside];
}

- (void)clickButtonAction
{
    NSString *url = @"";
    // NOTE: 将签名成功字符串格式化为订单字符串,请严格按照该格式
    [NetWorkingManager postWithUrl:url params:nil success:^(id json) {
        NSLog(@"%@",json);
        NSString *order = [NSString stringWithFormat:@"%@",json[@"result"]];
        [self payActionWith:order];
    } failure:^(NSDictionary *error) {
        
    }];

}

- (void)payActionWith:(NSString *)orderStr
{
    NSString *appScheme = @"alisVaccineInfo";

    // NOTE: 调用支付结果开始支付
    [[AlipaySDK defaultService] payOrder:orderStr fromScheme:appScheme callback:^(NSDictionary *resultDic) {
        NSLog(@"reslut = %@",resultDic);
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
