//
//  SystemShareVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/10/24.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "SystemShareVc.h"
#import <Masonry/Masonry.h>
#import "TSShareHelper.h"
@interface SystemShareVc ()<UIDocumentPickerDelegate>
@property (nonatomic, strong) UIButton *systemShareBtn;

@end

@implementation SystemShareVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.systemShareBtn];
    [self.systemShareBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view.mas_centerX);
        make.centerY.equalTo(self.view.mas_centerY);
        make.size.mas_equalTo(CGSizeMake(200, 40));
    }];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)clickSystemShareBtnAction
{
    //设置支持的文件类型
    NSArray *documentTypes = @[@"public.content", @"public.text", @"public.source-code ", @"public.image", @"public.audiovisual-content", @"com.adobe.pdf", @"com.apple.keynote.key", @"com.microsoft.word.doc", @"com.microsoft.excel.xls", @"com.microsoft.powerpoint.ppt"];

    //调起文件夹,
    UIDocumentPickerViewController *document = [[UIDocumentPickerViewController alloc] initWithDocumentTypes:documentTypes inMode:UIDocumentPickerModeOpen];
    document.delegate = self;
    [self presentViewController:document animated:YES completion:nil];
    
    
    

}

- (void)documentPicker:(UIDocumentPickerViewController *)controller didPickDocumentsAtURLs:(NSArray <NSURL *>*)urls NS_AVAILABLE_IOS(11_0)
{
    NSLog(@"获取文件夹内的内容====%@",urls);
    if(urls.count <=0){
        return;
    }
    NSString *url = [[urls objectAtIndex:0] absoluteString];
    
    [TSShareHelper shareWithType:TSShareHelperShareTypeOthers
                   andController:self
                     andFilePath:url
                   andCompletion:^(TSShareHelper *shareHelper, BOOL success) {

                       if (success) {
                           NSLog(@"成功的回调");
                       }else{
                           NSLog(@"失败的回调");
                       }
                   }];
}
- (void)documentPickerWasCancelled:(UIDocumentPickerViewController *)controller
{
    NSLog(@"点击取消按钮");
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIButton *)systemShareBtn{
    if(!_systemShareBtn){
        _systemShareBtn = ({
            UIButton * object = [[UIButton alloc]init];
            object.backgroundColor = [UIColor redColor];
            [object setTitle:@"点击系统分享" forState:UIControlStateNormal];
            [object addTarget:self action:@selector(clickSystemShareBtnAction) forControlEvents:UIControlEventTouchUpInside];
            object;
       });
    }
    return _systemShareBtn;
}
@end
