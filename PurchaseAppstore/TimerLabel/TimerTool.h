//
//  TimerTool.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/6/13.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void(^ClickGetCapthcaCodeBlock)(void);
NS_ASSUME_NONNULL_BEGIN

@interface TimerTool : UIView
@property (nonatomic, copy) ClickGetCapthcaCodeBlock clickGetCapthcaCodeBlock;

@end

NS_ASSUME_NONNULL_END
