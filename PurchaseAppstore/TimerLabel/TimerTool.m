//
//  TimerTool.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/6/13.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "TimerTool.h"
static NSString *const kResetTimeText = @"获取验证码";

@interface TimerTool ()
{
    NSInteger time;
    NSTimer *timer;
}
@property (nonatomic, strong) UILabel *timeLabel;

@end

@implementation TimerTool
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.timeLabel.frame = CGRectMake(0, 0, frame.size.width, frame.size.height);
        [self addSubview:self.timeLabel];
        self.timeLabel.layer.borderWidth = 1;
    }
    return self;
}

#pragma mark -----------倒计时
- (void)resetTimer
{
    [timer invalidate];
    timer = nil;
    [self handleTimeLabelStatus:YES];
    
    
}
- (void)startTimer
{
    [self handleTimeLabelStatus:NO];
    time = 59;
    timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerAction) userInfo:nil repeats:YES];
    
}

- (void)timerAction
{
    time -= 1;
    NSString *timestr = [NSString stringWithFormat:@"%lds后重新发送",(long)time];
    self.timeLabel.text = timestr;
    if (time == -1) {
        [timer invalidate];
        timer = nil;
        [self resetTimer];
    }
}

- (void)handleTimeLabelStatus:(BOOL)invaild
{
    self.timeLabel.userInteractionEnabled = invaild;
    self.timeLabel.text = invaild?kResetTimeText:@"59s后重新发送";
//    self.timeLabel.layer.borderColor =  invaild?kContentBlueColor.CGColor: kContentLightGrayColor.CGColor;
//    self.timeLabel.textColor = invaild?kContentBlueColor:kContentLightGrayColor;
}
- (void)clickGetAuthCodeGesture:(UITapGestureRecognizer *)tap
{
    [self startTimer];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (UILabel *)timeLabel{
    if(!_timeLabel){
        _timeLabel = ({
            UILabel * object = [[UILabel alloc] init];
            object.font = [UIFont systemFontOfSize:14];
//            object.textColor = kUIColorFromRGB(0x12D8F9);
            object.text = kResetTimeText;
            object.textColor = [UIColor blueColor];
            object.textAlignment = NSTextAlignmentRight;
            
            object.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickGetAuthCodeGesture:)];
            [object addGestureRecognizer:tap];
            object;
       });
    }
    return _timeLabel;
}
@end
