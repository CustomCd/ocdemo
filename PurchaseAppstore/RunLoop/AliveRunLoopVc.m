//
//  AliveRunLoopVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/2/14.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "AliveRunLoopVc.h"

@interface AliveRunLoopVc ()
@property (nonatomic, strong) NSThread *thread;

@end

@implementation AliveRunLoopVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIButton *testBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn setTitle:@"测试" forState:UIControlStateNormal];
    [testBtn setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [testBtn addTarget:self action:@selector(btnClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:testBtn];
    testBtn.frame = CGRectMake(100, 100, 100, 100);
    
    UIButton *testBtn1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [testBtn1 setTitle:@"开启线程" forState:UIControlStateNormal];
    [testBtn1 setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [testBtn1 addTarget:self action:@selector(startThrend) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:testBtn1];
    testBtn1.frame = CGRectMake(100, 300, 100, 100);

    NSLog(@"==========");
    
}

- (void)startThrend
{
    self.thread = [[NSThread alloc]initWithTarget:self selector:@selector(addRunloop) object:nil];
    [self.thread start];
}

- (void)addRunloop
{
    NSLog(@"task1--%@",[NSThread currentThread]);
    
    // 1.获得子线程对应的runloop
    NSRunLoop * currentRunLoop = [NSRunLoop currentRunLoop];
    
    /**
     问题：这里获得子线程对应的runloop如果只调用开启runloop方法，没有效果？
     原因：mode 里面至少要有一个timer(定时器事件) 或者是source(源);
     解决：添加timer事件 或 source源(采用)：
     分析：目地是保证runloop不退出，这里开启一个NSTimer定时器用浪费了。
     */
    // Mode1.添加timer事件
    [currentRunLoop addTimer:[NSTimer timerWithTimeInterval:2.0 target:self selector:@selector(testTimerMethod) userInfo:nil repeats:YES] forMode:NSDefaultRunLoopMode];
    
    // Mode2.添加Source1源(采用),基于端口的事件.
    //[currentRunLoop addPort:[NSPort port] forMode:NSDefaultRunLoopMode];
    
    // 2.一直运行runloop，
    [currentRunLoop run];
    
    // 只限用于子线程，启动Runloop并设置多少时间后退出，退出后会才会执行下面的代码、或指定运行模式.
//    [currentRunLoop runUntilDate:[NSDate dateWithTimeIntervalSinceNow:10.0]];
    
    // 这里的指定模式要和添加Sources源事件模式一致（这里只能切换执行一次，不知道为什么❓）
    //[currentRunloop runMode:NSDefaultRunLoopMode beforeDate:[NSDate dateWithTimeIntervalSinceNow:5.0]];
    
    NSLog(@"---验证Runloop开启循环让线程不被释放：Runloop退出--");
}

- (void)btnClick:(id)sender {
    // 这时调用start会crash掉，现在thread已经处于死亡状态
    //[self.thread start];
    
    // 线程间通信（再回到执行任务的线程）
    [self performSelector:@selector(test) onThread:self.thread withObject:nil waitUntilDone:NO];
}
-(void)test
{
    NSLog(@"测试让子线程持续存在,也可以切换执行其他任务 task2--%@",[NSThread currentThread]);

}
- (void)testTimerMethod
{
    NSLog(@"测试让子线程持续存在多长时间后退出，定时器销毁 taskThread %@, threadTime:%f",[NSThread currentThread],CFAbsoluteTimeGetCurrent());
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
