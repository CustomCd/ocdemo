//
//  RunLoopMainVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/2/14.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "RunLoopMainVc.h"

@interface RunLoopMainVc ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) UITableView *runloop_tableview;
@property (nonatomic, strong) NSArray *datas;

@end

@implementation RunLoopMainVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.runloop_tableview];
    
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.datas.count ;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSDictionary *data_dic = self.datas[indexPath.row];
    cell.textLabel.text = data_dic[@"name"];
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *data_dic = self.datas[indexPath.row];

    Class class = NSClassFromString(data_dic[@"vc"]);
    UIViewController *vc = [[class alloc] init];
    vc.title = data_dic[@"name"];
    [self.navigationController pushViewController:vc animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)runloop_tableview{
    if(!_runloop_tableview){
        _runloop_tableview = ({
            UITableView * object = [[UITableView alloc]initWithFrame:self.view.bounds];
            object.rowHeight = 44;
            object.delegate = self;
            object.dataSource = self;
            object;
       });
    }
    return _runloop_tableview;
}

- (NSArray *)datas{
    if(!_datas){
        _datas = ({
            NSArray * object = [[NSArray alloc]init];
            object = @[@{@"name":@"常驻线程",@"vc":@"AliveRunLoopVc"}];
            object;
       });
    }
    return _datas;
}
@end
