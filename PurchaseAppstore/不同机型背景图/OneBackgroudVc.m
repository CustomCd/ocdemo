//
//  OneBackgroudVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/9/27.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "OneBackgroudVc.h"
#import <Photos/Photos.h>
@interface OneBackgroudVc ()
@property (nonatomic, strong) UIImageView *backgroud;

@end

@implementation OneBackgroudVc
- (void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidDisappear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = NO;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.backgroud];
    [self.backgroud mas_makeConstraints:^(MASConstraintMaker *make) {
        make.size.mas_equalTo(CGSizeMake(150,250 ));
        make.center.equalTo(self.view);
    }];
}

- (void)clickTap:(UITapGestureRecognizer *)tap
{
    CGPoint point = [tap locationInView:self.backgroud];
    [self loadImageFinished:self.backgroud.image];
}
- (void)loadImageFinished:(UIImage *)image
{
    UIImageWriteToSavedPhotosAlbum(image, self, @selector(image:didFinishSavingWithError:contextInfo:), (__bridge void *)self);
}

- (void)image:(UIImage *)image didFinishSavingWithError:(NSError *)error contextInfo:(void *)contextInfo
{
    
    NSLog(@"image = %@, error = %@, contextInfo = %@", image, error, contextInfo);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIImageView *)backgroud{
    if(!_backgroud){
        _backgroud = ({
            UIImageView * object = [[UIImageView alloc]init];
            object.backgroundColor = [UIColor clearColor];
            UIImage *original_img = [UIImage imageNamed:@"11"];
            object.image =  original_img;
            object.layer.cornerRadius = 10;
            object.layer.borderWidth = 1;
            object.layer.masksToBounds = YES;
            object.userInteractionEnabled = YES;
            UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickTap:)];
            [object addGestureRecognizer:tap];
            object;
       });
    }
    return _backgroud;
}
@end
