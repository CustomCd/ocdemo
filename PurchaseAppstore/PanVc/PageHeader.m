//
//  PageHeader.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/4.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "PageHeader.h"
@interface PageHeader ()
{
    UIButton *_selectPageButton;
}
@property (nonatomic, strong) UIScrollView *scroll_bottom;

@end

@implementation PageHeader
- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.scroll_bottom.frame = self.bounds;
        [self addSubview:self.scroll_bottom];
    }
    return self;
}

- (void)setPageHeaderConfigWithContents:(NSArray *)content
{
    CGFloat item_width = [[UIScreen mainScreen] bounds].size.width/5;
    CGFloat item_height = self.scroll_bottom.frame.size.height;
    CGFloat item_x = 0;
    CGFloat item_y = 0;
    CGFloat item_space = 0;
    for (int i = 0; i<content.count; i++) {
        UIButton *pageButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [pageButton setTitle:content[i] forState:UIControlStateNormal];
        [pageButton setTitle:content[i] forState:UIControlStateHighlighted];
        [pageButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [pageButton setTitleColor:[UIColor orangeColor] forState:UIControlStateSelected];
        pageButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [pageButton addTarget:self action:@selector(clickPageButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.scroll_bottom addSubview:pageButton];
        pageButton.tag = i;
        item_x = (item_space+item_width)*i;
        pageButton.frame = CGRectMake(item_x, item_y, item_width, item_height);
        if (i == 0) {
            pageButton.selected = YES;
            _selectPageButton = pageButton;
        }

    }
}

- (void)clickPageButtonAction:(UIButton *)button
{
    if (button.tag ==_selectPageButton.tag) {
        return;
    }
    UIPageViewControllerNavigationDirection direction = button.tag > _selectPageButton.tag ?UIPageViewControllerNavigationDirectionForward:UIPageViewControllerNavigationDirectionReverse;

    _selectPageButton.selected = NO;
    button.selected = YES;
    _selectPageButton = button;
    if (self.clickButtonIndexBlock) {
        self.clickButtonIndexBlock(_selectPageButton.tag,direction);
    }
}

- (void)updatePageHeaderSelelctIndex:(NSInteger)index
{
    UIButton *selectButtton = self.scroll_bottom.subviews[index];
    if (selectButtton.tag ==_selectPageButton.tag) {
        return;
    }
    _selectPageButton.selected = NO;
    selectButtton.selected = YES;
    _selectPageButton = selectButtton;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


- (UIScrollView *)scroll_bottom{
    if(!_scroll_bottom){
        _scroll_bottom = ({
            UIScrollView * object = [[UIScrollView alloc]init];
            object;
       });
    }
    return _scroll_bottom;
}
@end
