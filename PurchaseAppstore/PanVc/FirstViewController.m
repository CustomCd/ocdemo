//
//  FirstViewController.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/5/20.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) NSArray *data_list;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.first_tableview];
    [self.first_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view);
    }];
}

- (void)updateTableviewData
{
    [self.first_tableview reloadData];
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data_list.count ;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.data_list[indexPath.row];
    
    return cell;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UITableView *)first_tableview{
    if(!_first_tableview){
        _first_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object;
       });
    }
    return _first_tableview;
}

- (NSArray *)data_list{
    if(!_data_list){
        _data_list = ({
            NSArray * object = @[@"路飞",@"索罗",@"香吉士",@"乌索普",@"娜美",@"罗宾",@"弗兰奇",@"乔巴",@"布鲁克"];
            
            object;
       });
    }
    return _data_list;
}
@end
