//
//  ScrollViewVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/5/20.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "ScrollViewVc.h"
#import "FirstViewController.h"
#import "SecondVc.h"
#import "ThirdVc.h"
#import "PageHeader.h"
@interface ScrollViewVc ()<UIScrollViewDelegate,UIPageViewControllerDelegate,UIPageViewControllerDataSource>
@property (nonatomic, strong) UIScrollView *page_scrollview;
@property (nonatomic, strong) FirstViewController *vc1;
@property (nonatomic, strong) SecondVc *vc2;
@property (nonatomic, strong) ThirdVc *vc3;
@property (nonatomic, strong) UIPageViewController *pageVc;
@property (nonatomic, strong) NSArray *vc_arr;
@property (nonatomic, strong) PageHeader *header;

@end

@implementation ScrollViewVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = @"page";
    [self.view addSubview:self.header];
    [self addPageVc];
    [self addChildViewController:self.pageVc];
    [self.view addSubview:self.pageVc.view];

    
}
- (void)addPageVc
{
    [self.pageVc setViewControllers:@[self.vc1]
                      direction:UIPageViewControllerNavigationDirectionReverse
                       animated:NO
                     completion:nil];
    
}
#pragma mark - UIPageViewControllerDataSource And UIPageViewControllerDelegate

#pragma mark 返回上一个ViewController对象

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController {
    
    NSInteger index =  [self.vc_arr indexOfObject:viewController];
    if ((index == 0) || (index == NSNotFound)) {
        //到第一个vc时 再往前设置成最后一个
        return [self.vc_arr lastObject];
    }
    index--;
//    NSLog(@"上一个===%@",viewController);

    return self.vc_arr[index];
    
    
}

#pragma mark 返回下一个ViewController对象

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController {
    
    NSInteger index =  [self.vc_arr indexOfObject:viewController];
    if (index == NSNotFound) {
        return nil;
    }
    index++;
    if (index == [self.vc_arr count]) {
        //到最后一个vc时 再往后设置成第一个
        return self.vc_arr[0];
    }
//    NSLog(@"下一个===%@",viewController);

    return self.vc_arr[index];
}
- (void)pageViewController:(UIPageViewController *)pageViewController willTransitionToViewControllers:(NSArray<UIViewController *> *)pendingViewControllers

{
    NSLog(@"willTransitionToViewController");
}
- (void)pageViewController:(UIPageViewController *)pageViewController didFinishAnimating:(BOOL)finished previousViewControllers:(NSArray<UIViewController *> *)previousViewControllers transitionCompleted:(BOOL)completed
{
    NSLog(@"didFinishAnimating===%@",pageViewController.viewControllers);
    
    NSInteger index =  [self.vc_arr indexOfObject:pageViewController.viewControllers[0]];
    [self.header updatePageHeaderSelelctIndex:index];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIScrollView *)page_scrollview{
    if(!_page_scrollview){
        _page_scrollview = ({
            UIScrollView * object = [[UIScrollView alloc]init];
            object.pagingEnabled = YES;
            object;
       });
    }
    return _page_scrollview;
}

- (UIPageViewController *)pageVc{
    if(!_pageVc){
        _pageVc = ({
            NSDictionary *options = @{UIPageViewControllerOptionInterPageSpacingKey : @(0)};
            // 根据给定的属性实例化UIPageViewController
            UIPageViewController * object = [[UIPageViewController alloc]initWithTransitionStyle:UIPageViewControllerTransitionStyleScroll navigationOrientation:UIPageViewControllerNavigationOrientationHorizontal
                            options:options];
            object.delegate = self;
            object.dataSource = self;
            object.view.frame = CGRectMake(0, 124, self.view.bounds.size.width, self.view.bounds.size.height-124);

            object;
       });
    }
    return _pageVc;
}

- (FirstViewController *)vc1{
    if(!_vc1){
        _vc1 = ({
            FirstViewController * object = [[FirstViewController alloc]init];
            object;
       });
    }
    return _vc1;
}

- (SecondVc *)vc2{
    if(!_vc2){
        _vc2 = ({
            SecondVc * object = [[SecondVc alloc]init];
            object;
       });
    }
    return _vc2;
}

- (ThirdVc *)vc3{
    if(!_vc3){
        _vc3 = ({
            ThirdVc * object = [[ThirdVc alloc]init];
            object;
       });
    }
    return _vc3;
}

- (NSArray *)vc_arr{
    if(!_vc_arr){
        _vc_arr = ({
            NSArray * object = [NSArray arrayWithObjects:self.vc1,self.vc2,self.vc3, nil];
            object;
       });
    }
    return _vc_arr;
}

- (PageHeader *)header{
    if(!_header){
        _header = ({
            PageHeader * object = [[PageHeader alloc]initWithFrame:CGRectMake(0, 64, self.view.bounds.size.width, 60)];
            [object setPageHeaderConfigWithContents:@[@"海贼王",@"一人之下",@"梦幻西游"]];
            __weak typeof(self)weakSelf = self;

            object.clickButtonIndexBlock = ^(NSInteger index, UIPageViewControllerNavigationDirection direction) {
                [weakSelf.pageVc setViewControllers:@[weakSelf.vc_arr[index]]
                                          direction:direction
                                           animated:YES
                                         completion:nil];
            };
            object;
       });
    }
    return _header;
}
@end
