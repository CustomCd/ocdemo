//
//  PageHeader.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/4.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef void(^ClickButtonIndexBlock)(NSInteger index,UIPageViewControllerNavigationDirection direction);
NS_ASSUME_NONNULL_BEGIN

@interface PageHeader : UIView
@property (nonatomic, copy) ClickButtonIndexBlock clickButtonIndexBlock;

- (void)setPageHeaderConfigWithContents:(NSArray *)content;
- (void)updatePageHeaderSelelctIndex:(NSInteger )index;
@end

NS_ASSUME_NONNULL_END
