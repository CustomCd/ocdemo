//
//  ThirdVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/7/3.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "ThirdVc.h"

@interface ThirdVc ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSArray *data_list;
@property (nonatomic, strong) UITableView *data_tableview;

@end

@implementation ThirdVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.data_tableview];
    [self.data_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view);
    }];
    
}

#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data_list.count ;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.data_list[indexPath.row];
    
    return cell;
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

- (NSArray *)data_list{
    if(!_data_list){
        _data_list = ({
            NSArray * object = @[@"剑侠客",@"飞燕女",@"逍遥生",@"晏无师",@"龙太子",@"神天兵"];
            object;
        });
    }
    return _data_list;
}


- (UITableView *)data_tableview{
    if(!_data_tableview){
        _data_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object;
       });
    }
    return _data_tableview;
}
@end
