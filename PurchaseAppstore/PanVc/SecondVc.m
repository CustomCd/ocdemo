//
//  SecondVc.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/5/20.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "SecondVc.h"

@interface SecondVc ()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic, strong) NSArray *data_list;

@end

@implementation SecondVc

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.view addSubview:self.second_tableview];
    [self.second_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(self.view);
    }];

}

- (void)updateTableviewData
{
    [self.second_tableview reloadData];
}
#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.data_list.count ;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = self.data_list[indexPath.row];
    
    return cell;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (UITableView *)second_tableview{
    if(!_second_tableview){
        _second_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.delegate = self;
            object.dataSource = self;
            object;
        });
    }
    return _second_tableview;
}

- (NSArray *)data_list{
    if(!_data_list){
        _data_list = ({
            NSArray * object = @[@"宝姐儿",@"张楚岚",@"王也",@"诸葛青"];
            
            object;
        });
    }
    return _data_list;
}
@end
