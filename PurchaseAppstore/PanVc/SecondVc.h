//
//  SecondVc.h
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2019/5/20.
//  Copyright © 2019年 亿苗通. All rights reserved.
//

#import "BaseVC.h"

NS_ASSUME_NONNULL_BEGIN

@interface SecondVc : BaseVC
@property (nonatomic, strong) UITableView *second_tableview;
- (void)updateTableviewData;
@end

NS_ASSUME_NONNULL_END
