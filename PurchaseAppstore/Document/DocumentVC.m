//
//  DocumentVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "DocumentVC.h"
#import "UIButton+Block.h"
@interface DocumentVC ()<UIWebViewDelegate,UIDocumentInteractionControllerDelegate>
{
    NSURL *filePath;
    NSInteger count;
    UIButton *fileBtn;
}
@property (nonatomic,strong) UITableView *tableView;
@property (nonatomic,strong) UITextView *textView;
@property (nonatomic,strong) UIImageView *imageView;
@property (nonatomic,assign) CGPoint gestureStartingPoint;
@property (nonatomic,assign) CGPoint gestureStartingCenter;
@property (nonatomic, strong) UIWebView *webview;
@end

@implementation DocumentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    fileBtn = [UIButton buttonWithType:UIButtonTypeCustom];
//    [fileBtn addTarget:self action:@selector(clickBtnAction) forControlEvents:UIControlEventTouchUpInside];
    
    [fileBtn setTitle:@"Doc" forState:UIControlStateNormal];
    fileBtn.layer.borderWidth = 1;
    [fileBtn setTitleColor:[UIColor cyanColor] forState:UIControlStateNormal];
    [self.view addSubview:fileBtn];
    [fileBtn block:^(id sender) {
        NSLog(@"222222");
    }];
    
    self.webview = [[UIWebView alloc] init];
    [self.view addSubview:self.webview];
    [fileBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.view).offset(64+20);
        make.size.mas_equalTo(CGSizeMake(100, 40));
        make.centerX.equalTo(self.view.mas_centerX);
    }];
    [self.webview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.view);
        make.top.equalTo(fileBtn.mas_bottom).offset(10);
    }];
    //    NSURL *filePath = [NSURL URLWithString:[[NSBundle mainBundle] pathForResource:@"账号备份" ofType:@"xlsx"]];
    //    NSURL *filePath = [NSURL URLWithString:@"https://www.tutorialspoint.com/ios/ios_tutorial.pdf"];
    filePath = [NSURL URLWithString:[[NSBundle mainBundle] pathForResource:@"interface" ofType:@"doc"]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL: filePath];
    [self.webview loadRequest:request];
    //使文档的显示范围适合UIWebView的bounds
    [self.webview setScalesPageToFit:YES];
    self.webview.delegate  =self;
    count = 1;
    
}

- (void)clickBtnAction
{
    count+= 1;
    
    NSString *path = @"";
    NSString *btn_str = @"";
    switch (count) {
        case 0:
            //            path = @"https://www.tutorialspoint.com/ios/ios_tutorial.pdf";
            path = [[NSBundle mainBundle] pathForResource:@"cc" ofType:@"pdf"];
            btn_str = @"PDF";
            
            break;
        case 1:
            path = [[NSBundle mainBundle] pathForResource:@"interface" ofType:@"doc"];
            btn_str = @"Doc";
            break;
        case 2:
            path = [[NSBundle mainBundle] pathForResource:@"aaa" ofType:@"xlsx"];
            btn_str = @"Xlsx";
            break;
        case 3:
            path = [[NSBundle mainBundle] pathForResource:@"tf03031002" ofType:@"potx"];
            btn_str = @"PPT";
            break;
    }
    [fileBtn setTitle:btn_str forState:UIControlStateNormal];
    filePath = [NSURL URLWithString:path];

    if (count == 3) {
        count = -1;
    }
    fileBtn.enabled = NO;
    [self.webview loadRequest:[NSURLRequest requestWithURL:filePath]];
    
//    NSURL *url = [NSURL fileURLWithPath:path];
//
//    UIDocumentInteractionController *documentVc = [UIDocumentInteractionController interactionControllerWithURL:url];
//    documentVc.delegate = self;
//
//    [documentVc presentPreviewAnimated:YES];

}

- (void)webViewDidStartLoad:(UIWebView *)webView
{
    NSLog(@"开始加载");
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSLog(@"加载完成");
    fileBtn.enabled = YES;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"加载失败====%@",error);
    fileBtn.enabled = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UIDocumentInteractionController 代理方法
- (UIViewController *)documentInteractionControllerViewControllerForPreview:(UIDocumentInteractionController *)controller{
    return self;
}

- (UIView *)documentInteractionControllerViewForPreview:(UIDocumentInteractionController *)controller{
    return self.view;
}

- (CGRect)documentInteractionControllerRectForPreview:(UIDocumentInteractionController *)controller{
    return self.view.bounds;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
