//
//  AppDelegate+GaoDeMap.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/5/21.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "AppDelegate+GaoDeMap.h"
#import <AMapFoundationKit/AMapFoundationKit.h>

const static NSString *kGaoDeAPIKey = @"ea8a4b4bf81f901e9cceac9535cdad79";

@implementation AppDelegate (GaoDeMap)
- (void)setGaoDeMapConfig
{
    [AMapServices sharedServices].apiKey = (NSString *)kGaoDeAPIKey;

}
@end
