//
//  UpMoveVC.m
//  PurchaseAppstore
//
//  Created by 亿苗通 on 2018/8/10.
//  Copyright © 2018年 亿苗通. All rights reserved.
//

#import "UpMoveVC.h"
#import <Masonry/Masonry.h>
#import "SecondVc.h"
@interface UpMoveVC ()<UITableViewDelegate,UITableViewDataSource,UIScrollViewDelegate>
@property (nonatomic, strong) UIView *header;
@property (nonatomic, strong) UITableView *up_tableview;

@end

@implementation UpMoveVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    [self.view addSubview:self.header];
//    [self.view addSubview:self.up_tableview];
//    [self.up_tableview mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.right.bottom.equalTo(self.view);
//        make.top.equalTo(self.view).offset(0);
//    }];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(receiveNotification) name:@"text" object:nil];
}
- (void)receiveNotification
{
    NSLog(@"收到通知了");
}
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event

{
    [self presentViewController:[[SecondVc alloc]init] animated:YES completion:nil];
}

#pragma mark TableviewDelegate datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 20;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *indentifier = @"UITableViewCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:indentifier];
    
    if (nil == cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:indentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = [NSString stringWithFormat:@"%ld",indexPath.row];
    
    return cell;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView
{
    NSLog(@"%@",scrollView);
}
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat offsetY = scrollView.contentOffset.y;
    
    //上拉加载更多（头部还没有隐藏），动态移动header
    if (offsetY > -200 & offsetY < 0) {
        

        self.header.frame =CGRectMake(0, -offsetY-200, 375, 200);

        
    }else if(offsetY > 0){ //头部隐藏，固定头部位置
        

        self.header.frame =CGRectMake(0, -200, 375, 200);

    }else{ //下拉刷新

        self.header.frame =CGRectMake(0, 0, 375, 200);

    }


}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


- (UIView *)header{
    if(!_header){
        _header = ({
            UIView * object = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 375, 200)];
            object.backgroundColor = [UIColor cyanColor];
            object;
       });
    }
    return _header;
}

- (UITableView *)up_tableview{
    if(!_up_tableview){
        _up_tableview = ({
            UITableView * object = [[UITableView alloc]init];
            object.backgroundColor = [UIColor orangeColor];
            object.delegate = self;
            object.dataSource = self;
            object.rowHeight = 44;
            //设置内容偏移
            object.contentInset = UIEdgeInsetsMake(200, 0, 0, 0);
            //设置滚动条偏移
            object.scrollIndicatorInsets = UIEdgeInsetsMake(200, 0, 0, 0);

            object;
       });
    }
    return _up_tableview;
}
@end
